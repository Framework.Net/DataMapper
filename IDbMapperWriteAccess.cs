﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;


using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace DataMapper
{
    /// <summary>
    /// This is the write access of interface IDbMapped.
    /// Contains all methods that alter data in database...
    /// </summary>
    public partial interface IDbMapperWriteAccess
    {
        #region DbMapper_Item

        void CreateInDatabase<TObject>(TObject item, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;

        int UpdateToDatabase<TObject>(TObject item, bool with_transaction = true, string sqlCondition = null, IReadOnlyCollection<DbMappedField> fieldsToUpdate = null)
            where TObject : IAutoMappedDbObject;
        int UpdateToDatabase<TObject>(TObject item, string sqlCondition, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject;
        int UpdateToDatabase<TObject>(TObject item, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject;
        int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<PropertyInfo> propertiesToUpdate)
            where TObject : IAutoMappedDbObject;
        int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<DbMappedField> fieldsToUpdate)
            where TObject : IAutoMappedDbObject;

        void DeleteInDatabase<TObject>(TObject item, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;


        #endregion DbMapper_Item

        #region DbMapper_List

        void CreateInDatabaseCollectionSlow<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;
        void CreateInDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;
        void CreateInDatabaseCollection(IReadOnlyCollection<IAutoMappedDbObject> items, bool with_transaction = true);

        int UpdateToDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject;

        void DeleteInDatabaseCollection<TObject, TKey>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : BaseDTO<TKey>, IAutoMappedDbObject
            where TKey : struct, IEquatable<TKey>, IIdTupleWithOnlyOneKey;

        void Truncate<TObject>()
            where TObject : IAutoMappedDbObject;

        #endregion DbMapper_List

        #region Old (Legacy code that use "state")


        #region DbMapper_Item

        void SynchronizeToDatabase<TObject>(TObject item)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject;

        #region Sucre syntaxique

        void Save<T>(T item)
            where T : IPropertyNotifierObject, IAutoMappedDbObject;
        void Delete<T>(T item)
            where T : IPropertyNotifierObject, IAutoMappedDbObject;
        #endregion

        #endregion DbMapper_Item


        #region DbMapper_List

        void AutoSaveCollection<TObject>(CompositeList<TObject> collection)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new();

        void AutoSaveItems<TObject>(IEnumerable<TObject> collection)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new();

        #endregion  DbMapper_List

        #endregion
    }
}
