﻿using System;


namespace DataMapper
{
    public interface IHasMapperOwner
    {
        IDbMapper Mapper { get; }
    }
}
