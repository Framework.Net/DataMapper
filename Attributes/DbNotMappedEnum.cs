﻿using System;


namespace DataMapper
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DbNotMappedEnumAttribute : DbMappedTable
    {
        public DbNotMappedEnumAttribute()
            : base(null, null)
        {
        }
    }

}
