﻿using System;
using System.Diagnostics;
using System.Linq;


namespace DataMapper
{
    /// <summary>
    /// Une composition est un lien de structuration de donnée. Un objet a besoin d'un autre autre pour exister
    /// Et l'existence de chaque object est corrélé.
    /// Un livre ne peut exister sans page. Et si on detruit un livre, on detruit aussi ses pages
    /// Dans l'exemple ci dessous, le lien indique que le livre contient des pages dont l'existence est lié
    /// Ce lien n'empeche pas un autre objet de referencer une page via un lien d'aggregation.
    /// Par exemple une page d'un livre pourrait contenir une reference vers une autre page pour indiquer l'origine d'une citation.
    /// Dans ce cas ce sera à la couche métier de vérifier tout ça.
    /// Exemple :
    /// <example>
    /// <code>
    /// public class Book : IAutoMappedDbObject
    /// {
    ///     // Here we are saying that it is the table Pages (class Page) that contains the relation
    ///     [DbComposition(ReferencingType = typeof(Page), FkProperty = nameof(Page.BookId)]
    ///     public  List&lt;Page&gt; Pages { get; set;}
    ///
    ///     [...]
    /// }
    /// </code>
    /// </example>
    /// <example>
    /// Another example where application handle only one address by person.
    /// In this case, the Person table _could_ containt the reference to the address.
    /// <code>
    /// public class Person : IAutoMappedDbObject
    /// {
    ///     [DbMappedField([...])]
    ///     public  int AddressId { get; set;}
    ///
    ///     // Here we are saying that the Person instance owns the Address instance
    ///     // Note that ReferencingType = typeof(Person) is not mandattory because it is the defaut
    ///     [DbComposition(ReferencingType = typeof(Person), FkProperty = nameof(AddressId)]
    ///     public  Address Address { get; set; }
    ///
    ///     // Another way is to say the address contains the person that it belongs to
    ///     // In that case the referencingType is mandatory
    ///     [DbComposition(ReferencingType = typeof(Address), FkProperty = nameof(PersonOwnerId)]
    ///     public  Address Address { get; set; }
    ///
    ///     [...]
    /// }
    /// </code>
    /// </example>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]

    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    //[DebuggerDisplay(nameof(Table)            + "={" + nameof(Table)            + "?." + nameof(DbMappedTable.FullName)+ ",nq}, " +
    //                 nameof(ReferencingClass) + "={" + nameof(ReferencingClass) + "?." + nameof(Type.Name) + ",nq}, " +
    //                 nameof(FkPropertyName)   + "={" + nameof(FkPropertyName)   + "." + nameof(System.Reflection.PropertyInfo.Name) + ",nq}")]
    public class DbComposition : DbMappedPropertyAttribute
    {
        // Class that _technically_ holds the relation but this class not necessarly owns the relation
        public Type          ReferencingClass { get; set; }
        // Property name of this class on which a DbMappedField (with FkTo) has been / must be declared
        public string        FkPropertyName   { get; set; }
        // Will be filled by the engine
        public DbMappedTable ReferencingTable { get; internal set; }
        // KeyField belongs to the table related to ReferencingClass
        public DbMappedField KeyField         { get; internal set; }

        public string Name
        {
            get
            {
                return "Composition settled on " + Table.MappedType.Name + TechnicalRelation;
            }
        }
        internal string TechnicalRelation
        {
            get
            {
                bool ownerIsReferencing = ReferencingClass == Table.MappedType;
                return " (FK: " + (ownerIsReferencing ? "" : ReferencingClass.Name + ".") + KeyField.PropertyInfo.Name
                     + " => " + (ownerIsReferencing ? KeyField.ForeignKeys.Single().To.Name + "." + Table.Mapper.GetTableMapping(KeyField.ForeignKeys.Single().To, false)?.PkField.PropertyInfo.Name
                                                    : Table.PkField.PropertyInfo.Name) + ")";

            }
        }
    }

    /// <summary>
    /// First, read about DbComposition.
    /// DbCompositionReverse is for the reverse property.
    /// For example The DbComposition allow developper to create Bool class that contains Pages
    /// The natural way to use attributes is to put DbComposition on the "Pages" property of books
    /// However you may also need to have a Book object on Page class.
    /// In this case, use DbCompositionReverse to mark the Owner of a class.
    /// <remarks>The natural way (the property Pages) _must_ exist in order for you to be able to use DbCompositeReverse</remarks>
    /// <example>
    /// <code>
    /// public class Page : IAutoMappedDbObject
    /// {
    ///     [DbMappedField([...])]
    ///     public  int BookId { get; set;}
    ///
    ///     // Here we are saying this relation is the revese of Book.Pages
    ///     [DbCompositionReverse(OwnerClass = typeof(Book), CompositePropertyName = nameof(Pages)]
    ///     public  Book Book { get; set; }
    ///
    ///     [...]
    /// }
    /// </code>
    /// </example>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    //[DebuggerDisplay(nameof(Table) + "={" + nameof(Table) + "?." + nameof(DbMappedTable.FullName) + ",nq}, " +
    //                 nameof(OwnerClass) + "={" + nameof(OwnerClass) + "." + nameof(Type.Name) + ",nq}" +
    //                 nameof(CompositionRelation) + "={" + nameof(CompositionRelation) + ",nq}")]
    public class DbCompositionReverse : DbMappedPropertyAttribute
    {
        // Class that owns the composite relation in the first place
        public Type          OwnerClass              { get; set; }
        // Property of owning class that represents the relation and has a DbComposition attribute
        public string        CompositionPropertyName { get; set; }
        // Will be filled by the engine
        public DbComposition CompositionRelation     { get; internal set; }

        public string Name { get { return "Settled on " + Table.MappedType.Name + ", Reverse of " + CompositionRelation.Name; } }
    }
}
