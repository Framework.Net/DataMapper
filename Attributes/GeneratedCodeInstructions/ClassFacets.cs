﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

using TechnicalTools;


namespace DataMapper
{
    /// <summary> Group all class generation switches. For internal use </summary>
    [Serializable]
    public class ClassFacets : PropertyFacets // Currently inherits because, with attribute, we can't use composition by using a property "DefaultPropertySettings"
    {
        /// <summary> Indicate the class concerned by this attribute </summary>
        /// <remarks>Hidden because exposed as <see cref="GeneratedClassAttribute.For">For</see> property </remarks>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string ForClass { get; set; }
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string ForSchema { get; protected internal set; } = "Dbo";

        [IsFacet]
        public eClassKind     BaseClassKind                           { get => GetClassFacetValue<eClassKind>(); protected set => SetClassFacetValue(value); }

        [IsFacet]
        public Type           BaseClassType                           { get => GetClassFacetValue<Type>(); set => SetClassFacetValue(value); }

        /// <summary> Kind of interface to generate. </summary>
        [IsFacet]
        public eInterfaceKind GenerateInterface                       { get => GetClassFacetValue<eInterfaceKind>(); set => SetClassFacetValue(value); }

        /// <summary>
        /// Make all mapped properties to be initialized so the instance is immeditaly insertable in database.
        /// Useful for non null string for exemple. Will use DefaultValue of each property or default value of type allowed by remote database, or default value of C# type
        /// </summary>
        [IsFacet]
        public bool           GenerateConstructors                    { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }

        /// <summary>
        /// Name of property which is bound to identity column using a DbMappedFieldAttribute
        /// If value is null, default identity column is used at generation time.
        /// If no identity column exists, any unique integer type column is used.
        /// For all other cases an exception is raised at generation time.
        /// </summary>
        [IsFacet]
        public string[]       IdentityKeys                            { get => GetClassFacetValue<string[]>(); set => SetClassFacetValue(value); }
        /// <summary> Copy identity in cloning code. </summary>
        [IsFacet]
        public bool           CopyIdentityWhenCloning                 { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        /// <summary> Make class inherits from IHasOptimizedLoading and add implementation. </summary>
        [IsFacet]
        public bool           ImplementFastLoading                    { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        [IsFacet]
        public string         RelativePath                            { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        /// <summary> Legacy </summary>
        [IsFacet]
        public bool           GenerateReferencedCollectionProperties  { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        /// <summary> Legacy </summary>
        [IsFacet]
        public bool           GenerateReferencingCollectionProperties { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        /// <summary> Include experimental Linq2Sql features. </summary>
        [IsFacet]
        public bool           IncludeLinq2SqlFeatures                 { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }

        #region if BaseClassKind is DynamicEnum

        /// <summary>
        /// Property name which stores the business name of enum.
        /// If defined property is not a mapped property but a custom one, the generated setter will throw an exception at runtime
        /// A custom property is usually used to concatenate hierarchical names : familyName "/" + subFamilyName + "/" + name.
        /// </summary>
        [IsFacet]
        public string         BusinessNamePropertyName                { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }

        /// <summary> Raise no warning for developper when at runtime we find more item in database than declared in code </summary>
        [IsFacet]
        public bool           ListCanBeExtendedInDatabase             { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }

        /// <summary> Implement (both generic and not generic) IComparable interfaces using ordinal comparison on businessname </summary>
        [IsFacet]
        public bool           GenerateIComparable                     { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }

        //public static string[] AllAdditionaProperties = new string[] { nameof(BusinessNamePropertyName), nameof(ListCanBeExtendedInDatabase), nameof(GenerateIComparable) };

        #endregion

        public    IReadOnlyDictionary<string, object>   ClassFacetValues { get { return  _ClassFacetValues; } }
        protected          Dictionary<string, object>  _ClassFacetValues { get { return __ClassFacetValues ?? (__ClassFacetValues = new Dictionary<string, object>()); } }
           [NonSerialized] Dictionary<string, object> __ClassFacetValues;
        public    void ClearClassFacetValues()
        {
            if (__ClassFacetValues != null)
                _ClassFacetValues.Clear();
        }
        protected void SetClassFacetValue<T>(T value, [CallerMemberName] string facetName = null)
        {
            _ClassFacetValues[facetName] = value;
            OnClassFacetValueChanged?.Invoke(facetName);
        }
        protected event Action<string> OnClassFacetValueChanged;
        protected T    GetClassFacetValue<T>(         [CallerMemberName] string facetName = null)
        {
            if (_ClassFacetValues.TryGetValue(facetName, out object v))
                return v is T ? (T)v : default(T);
            return Parent != null
                 ? Parent.GetClassFacetValue<T>(facetName)
                 : default(T);
        }



        public new AssemblyFacets              Parent              { get { return (AssemblyFacets)base.Parent; } }
        public     Type                        DeclaringClassType  { get;                        protected internal set; }
        public new DbMappedTable               Mapping             { get;                        protected internal set; }
        public IReadOnlyList<PropertyFacets>   Properties          { get { return _Properties; } protected internal set { __Properties = value?.ToList() ?? new List<PropertyFacets>(); } }
        protected       List<PropertyFacets>  _Properties          { get { return __Properties ?? (__Properties = new List<PropertyFacets>()); } }
                        List<PropertyFacets> __Properties;


        protected internal ClassFacets(AssemblyFacets parent) : base(parent) { }

        public virtual ClassFacets Clone(AssemblyFacets parent)
        {
            var clone = CreateNewClassFacets(parent);
            clone.CopyFrom(this);
            return clone;
        }
        public virtual  ClassFacets    CreateNewClassFacets   (AssemblyFacets parent) { return new ClassFacets(parent); }

        public override void ClearAll()
        {
            base.ClearAll();
            ClearClassFacetValues();
            if (__Properties != null)
                __Properties.Clear();
        }
        // No override of CopyFrom(PropertyFacets pf) because we actually would like
        // ClassFacets to be "composed" of PropertyFacets, not inherit from it, so static type inference chose the "good" CopyFrom
        public virtual ClassFacets CopyFrom(ClassFacets cf)
        {
            base.CopyFrom(cf); // see this as a copy of a composite
            DeclaringClassType = cf.DeclaringClassType;
            Mapping = cf.Mapping;
            ClearClassFacetValues();
            MergeWith(cf.ClassFacetValues);
            __Properties = cf.Properties.Select(pf => pf.Clone(this)).ToList();
            return this;
        }
        protected internal virtual ClassFacets FindMatchAmong(IReadOnlyCollection<ClassFacets> amongClasses)
        {
            return amongClasses.FirstOrDefault(c => c.Mapping != null && Mapping != null
                                                  ? c.Mapping.FullName == Mapping.FullName
                                                  : c.DeclaringClassType != null && DeclaringClassType != null &&
                                                         c.DeclaringClassType?.FullName.Split('.').TakeLast(2).Join(".")
                                                    ==     DeclaringClassType?.FullName.Split('.').TakeLast(2).Join("."));
        }
        public virtual ClassFacets MergeWith(ClassFacets cf, bool forbidOverwrite = false)
        {
            base.MergeWith(cf, forbidOverwrite);
            MergeWith(cf.ClassFacetValues, forbidOverwrite);
            foreach (var p in cf.Properties)
            {
                var existingP = p.FindMatchAmong(_Properties);
                if (existingP == null)
                    _Properties.Add(p.Clone(this));
                else
                    existingP.MergeWith(p, forbidOverwrite);
            }
            return this;
        }
        protected internal new ClassFacets MergeWith(IReadOnlyDictionary<string, object> classFacetValues, bool forbidOverwrite = false)
        {
            foreach (var cfv in classFacetValues)
            {
                if (forbidOverwrite &&
                    _ClassFacetValues.ContainsKey(cfv.Key) &&
                    !Equals(_ClassFacetValues[cfv.Key], cfv.Value))
                    throw new Exception($"Some class facet is defined multiple times at different location in your assemblies with different values: " + Environment.NewLine +
                                        $"Key \"{cfv.Key}\" is defined at least twice with values \"{(_ClassFacetValues[cfv.Key] ?? "NULL")}\" and \"{(cfv.Value ?? "NULL")}\"");
                if (!(cfv.Value is Array) ||
                    ((Array)cfv.Value).Length > 0 ||
                    !_ClassFacetValues.ContainsKey(cfv.Key))
                    _ClassFacetValues[cfv.Key] = cfv.Value;
            }
            return this;
        }
    }
    public enum eClassKind
    {
        None,
        BaseDTO,
        SmallTableInMemory,
        DynamicEnum,
    }
    public enum eInterfaceKind
    {
        No,
        Yes,
        YesPlusReadOnly,
    }

}
