﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;


namespace DataMapper
{
    /// <summary> Group all class generation switches. For internal use </summary>
    [Serializable]
    public class AssemblyFacets : ClassFacets // Currently inherits because, with attribute, we can't use composition by using a property "DefaultClassSettings"
    {
        /// <summary> Indicate the assembly concerned by this attribute </summary>
        /// <remarks>Hidden because exposed as <see cref="GeneratedAssemblyAttribute.For">For</see> property </remarks>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string ForAssembly { get; protected internal set; }

        [IsFacet]
        public     string     AssemblyName                  { get => GetAssemblyFacetValue<string>(); set => SetAssemblyFacetValue(value); }
        [IsFacet]
        public new eClassKind BaseClassKind                 { get => base.BaseClassKind; set => base.BaseClassKind = value; }
        [IsFacet]
        public     bool       OneClassWithInterfacesPerFile { get => GetAssemblyFacetValue<bool>(); set => SetAssemblyFacetValue(value); }
        [IsFacet]
        public     bool       DoNotUseSubFolderForSchema    { get => GetAssemblyFacetValue<bool>(); set => SetAssemblyFacetValue(value); }
        [IsFacet]
        public     bool       TrackFileName                 { get => GetAssemblyFacetValue<bool>(); set => SetAssemblyFacetValue(value); }
        [IsFacet]
        public     bool       GenerateDAOClass              { get => GetAssemblyFacetValue<bool>(); set => SetAssemblyFacetValue(value); }
        [IsFacet]
        public     bool       GenerateDTOBaseClass          { get => GetAssemblyFacetValue<bool>(); set => SetAssemblyFacetValue(value); }
        [IsFacet]
        public     bool       ForSqlCE                      { get => GetAssemblyFacetValue<bool>(); set => SetAssemblyFacetValue(value); }

        public    IReadOnlyDictionary<string, object>   AssemblyFacetValues { get { return  _AssemblyFacetValues; } }
        protected          Dictionary<string, object>  _AssemblyFacetValues { get { return __AssemblyFacetValues ?? (__AssemblyFacetValues = new Dictionary<string, object>()); } }
           [NonSerialized] Dictionary<string, object> __AssemblyFacetValues;
        public    void ClearAssemblyFacetValue()
        {
            if (__AssemblyFacetValues != null)
                _AssemblyFacetValues.Clear();
        }
        protected void SetAssemblyFacetValue<T>(T value, [CallerMemberName] string facetName = null)
        {
            _AssemblyFacetValues[facetName] = value;
            OnAssemblyFacetValueChanged?.Invoke(facetName);
        }
        protected event Action<string> OnAssemblyFacetValueChanged;
        protected T    GetAssemblyFacetValue<T>(         [CallerMemberName] string facetName = null)
        {
            if (!_AssemblyFacetValues.TryGetValue(facetName, out object v))
                v = default(T);
            return v is T ? (T)v : default(T);
        }



        public     Assembly                 Assembly { get;                      protected internal set; }
        public IReadOnlyList<ClassFacets>   Classes  { get { return  _Classes; } protected internal set { __Classes = value?.ToList() ?? new List<ClassFacets>(); } }
        protected       List<ClassFacets>  _Classes  { get { return __Classes ?? (__Classes = new List<ClassFacets>()); } }
                        List<ClassFacets> __Classes;

        protected internal AssemblyFacets()
            : base(null)
        {
            // Prevent setting this default value for PropertyFacets
            Accessibility = MemberAttributes.Public;
        }

        public virtual AssemblyFacets Clone()
        {
            var clone = CreateNewAssemblyFacets();
            clone.CopyFrom(this);
            return clone;
        }
        public virtual  AssemblyFacets CreateNewAssemblyFacets() { return new AssemblyFacets(); }

        public override void ClearAll()
        {
            base.ClearAll();
            ClearAssemblyFacetValue();
            if (__Classes != null)
                __Classes.Clear();
        }
        // No override of CopyFrom(ClassFacets pf) because we actually would like
        // AssemblyFacets to be "composed" of ClassFacets, not inherit from it, so static type inference chose the "good" CopyFrom
        public virtual AssemblyFacets CopyFrom(AssemblyFacets af)
        {
            base.CopyFrom(af); // see this as a copy of a composite
            Assembly = af.Assembly;
            ClearAssemblyFacetValue();
            MergeWith(af.AssemblyFacetValues);
            __Classes = af.Classes.Select(cf => cf.Clone(this)).ToList();
            return this;
        }
        public     virtual AssemblyFacets MergeWith(AssemblyFacets af, bool forbidOverwrite = false, Func<ClassFacets, IReadOnlyCollection<ClassFacets>, ClassFacets> matchAmongOwnedClasses = null)
        {
            base.MergeWith(af, forbidOverwrite);
            MergeWith(af.AssemblyFacetValues, forbidOverwrite);
            foreach (var c in af.Classes)
            {
                var existingC = matchAmongOwnedClasses?.Invoke(c, _Classes)
                             ?? c.FindMatchAmong(_Classes);
                if (existingC == null)
                    _Classes.Add(c.Clone(this));
                else
                    existingC.MergeWith(c, forbidOverwrite);
            }
            return this;
        }
        protected internal new AssemblyFacets MergeWith(IReadOnlyDictionary<string, object> assemblyFacetValues, bool forbidOverwrite = false)
        {
            foreach (var afv in assemblyFacetValues)
            {
                if (forbidOverwrite &&
                    _AssemblyFacetValues.ContainsKey(afv.Key) &&
                    !Equals(_AssemblyFacetValues[afv.Key], afv.Value))
                    throw new Exception($"An assembly facet is defined multiple times at different location in your assemblies with different values: " + Environment.NewLine +
                                        $"Key \"{afv.Key}\" is defined at least twice with values \"{(_AssemblyFacetValues[afv.Key] ?? "NULL")}\" and \"{(afv.Value ?? "NULL")}\"");
                if (!(afv.Value is Array) ||
                    ((Array)afv.Value).Length > 0 ||
                    !_AssemblyFacetValues.ContainsKey(afv.Key))
                    _AssemblyFacetValues[afv.Key] = afv.Value;
            }
            return this;
        }
    }
}
