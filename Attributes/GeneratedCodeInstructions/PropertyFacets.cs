﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;


namespace DataMapper
{
    /// <summary> Group all property generation switches. For internal use </summary>
    [Serializable]
    public class PropertyFacets : Attribute
    {
        /// <summary>
        /// For internal or protected usage
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class IsFacetAttribute : Attribute
        {
            public bool   Obsolete { get; set; }
            public string Message  { get; set; }

            public int DeclaredAtLine { get; private set; }

            public IsFacetAttribute([CallerLineNumber] int declaredAtLine = 0)
            {
                DeclaredAtLine = declaredAtLine;
            }
        }

        /// <summary> Indicate the property concerned by this attribute </summary>
        /// <remarks>Hidden because exposed as <see cref="GeneratedPropertyAttribute.For">For</see> property </remarks>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [IsFacet]
        public string           ForProperty                   { get; protected internal set; }

        /// <summary> Tell generator to ignore this column (as if it does not exist). </summary>
        [IsFacet]
        public bool             IgnoreColumn                  { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary>
        /// Property accessors are written manually by developper,
        /// so generator does not generate them but will use the property elsewhere.
        /// </summary>
        [IsFacet]
        public bool             ManualAccessor                { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary> Just the default value to use in constructor </summary>
        [IsFacet]
        public object           DefaultValue                  { get => GetPropertyFacetValue<object>(); set => SetPropertyFacetValue(value); }

        /// <summary> Comment to put on generated property </summary>
        [IsFacet]
        public string           Comment                       { get => GetPropertyFacetValue<string>(); set => SetPropertyFacetValue(value); }

        /// <summary> If not blank, mark property with Obsolete attribute with "error" arg to true. </summary>
        [IsFacet]
        public string           ObsoleteMessage               { get => GetPropertyFacetValue<string>(); set => SetPropertyFacetValue(value); }

        /// <summary> Property will be marked with IsTechnicalPropertyAttribute </summary>
        [IsFacet]
        public bool             IsTechnicalProperty           { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary> Indicate if the value must be trimmed in the setter </summary>
        [IsFacet]
        public bool             TrimValue                     { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary> Indicate if the value must be trimmed and, if empty, nullified in the setter </summary>
        [IsFacet]
        public bool             TrimValueAndNullify           { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary> Visibility of property </summary>
        [IsFacet]
        public MemberAttributes Accessibility                 { get => GetPropertyFacetValue<MemberAttributes>(); set => SetPropertyFacetValue(value); }

        /// <summary>
        /// Generate changed event for property designed by For.
        /// The name of event is For + "Changed" and its type is EventHandler
        /// </summary>
        [IsFacet]
        public bool             GenerateSpecificEventOnChange { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary>
        /// Only valid if base class has BaseDTO as base ancestor class
        /// Will use one the methods <see cref="BaseDTO.SetProperty{T}(ref T, T)"/> or <see cref="BaseDTO.SetTechnicalProperty{T}(ref T, T, Action)"/>.
        /// Won't use <see cref="INotifyPropertyChanged"/>
        /// See events in <see cref="BaseDTO"/>
        /// </summary>
        [IsFacet]
        public bool             UseNotifyPropertyChanged      { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary>
        /// Make mapper to convert the real datatype from database to another valid type.
        /// Often used to "project" any database integer types to C# enum types,
        /// or db signed type to unsigned C# type..
        /// Default convenient projection exists. For example a char(1) is project to char even if db type is string.
        /// This type can also be a reference type that inherits from DynamicEnumWithId&lt;,&gt;, evne if the type is form another assembly
        /// </summary>
        [IsFacet]
        public Type             ProjectedType                     { get => GetPropertyFacetValue<Type>(); set => SetPropertyFacetValue(value); }

        /// <summary>
        /// Only valid if parent.Base is generated class inherit from BaseDTO (aka if Parent.BaseClassKind is at least eClassKind.BaseDTO)
        /// </summary>
        [IsFacet]
        public bool             CheckValueInSetter            { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }

        /// <summary>
        /// Add attribute to avoid entering property code in debugging
        /// </summary>
        [IsFacet]
        public bool             SkipInDebug                   { get => GetPropertyFacetValue<bool>(); set => SetPropertyFacetValue(value); }


        public IReadOnlyDictionary<string, object>   PropertyFacetValues { get { return  _PropertyFacetValues; } }
        protected          Dictionary<string, object>  _PropertyFacetValues { get { return __PropertyFacetValues ?? (__PropertyFacetValues = new Dictionary<string, object>()); } }
           [NonSerialized] Dictionary<string, object> __PropertyFacetValues;
        public    void ClearPropertyFacetValues()
        {
            if (__PropertyFacetValues != null)
                _PropertyFacetValues.Clear();
        }
        protected void SetPropertyFacetValue<T>(T value, [CallerMemberName] string facetName = null)
        {
            _PropertyFacetValues[facetName] = value;
            OnPropertyFacetValueChanged?.Invoke(facetName);
        }
        protected event Action<string> OnPropertyFacetValueChanged;
        protected T    GetPropertyFacetValue<T>(         [CallerMemberName] string facetName = null)
        {
            if (_PropertyFacetValues.TryGetValue(facetName, out object v))
                return v is T ? (T)v : default(T);
            return Parent != null
                 ? Parent.GetPropertyFacetValue<T>(facetName)
                 : default(T);
        }



        public ClassFacets   Parent   { get { return _Parent; } } [NonSerialized] readonly ClassFacets _Parent;
        public PropertyInfo  Property { get; protected internal set; }
        public DbMappedField Mapping  { get; protected internal set; }

        protected internal PropertyFacets(ClassFacets parent) { _Parent = parent; }

        public virtual PropertyFacets Clone(ClassFacets parent)
        {
            var clone = CreateNewPropertyFacets(parent);
            clone.CopyFrom(this);
            return clone;
        }
        public virtual PropertyFacets CreateNewPropertyFacets(ClassFacets parent) { return new PropertyFacets(parent); }
        public virtual void ClearAll()
        {
            ClearPropertyFacetValues();
        }
        public virtual PropertyFacets CopyFrom(PropertyFacets pf)
        {
            Property = pf.Property;
            Mapping = pf.Mapping;
            ClearPropertyFacetValues();
            MergeWith(pf.PropertyFacetValues);
            return this;
        }
        protected internal virtual PropertyFacets FindMatchAmong(IReadOnlyCollection<PropertyFacets> amongProperties)
        {
            return amongProperties.FirstOrDefault(c => Mapping != null && c.Mapping != null
                                                     ? Mapping.ColumnName == Mapping.ColumnName
                                                     : Property.Name == Property.Name);
        }
        public virtual PropertyFacets MergeWith(PropertyFacets pf, bool forbidOverwrite = false)
        {
            MergeWith(pf.PropertyFacetValues, forbidOverwrite);
            return this;
        }
        protected internal PropertyFacets MergeWith(IReadOnlyDictionary<string, object> propertyFacetValues, bool forbidOverwrite = false)
        {
            foreach (var pfv in propertyFacetValues)
            {
                if (forbidOverwrite &&
                    _PropertyFacetValues.ContainsKey(pfv.Key) &&
                    !Equals(_PropertyFacetValues[pfv.Key], pfv.Value))
                    throw new Exception($"A property facet is defined multiple times at different location in your assemblies with different values: " + Environment.NewLine +
                                        $"Key \"{pfv.Key}\" is defined at least twice with values \"{(_PropertyFacetValues[pfv.Key] ?? "NULL")}\" and \"{(pfv.Value ?? "NULL")}\"");
                if (!(pfv.Value is Array) ||
                    ((Array)pfv.Value).Length > 0 ||
                    !_PropertyFacetValues.ContainsKey(pfv.Key))
                    _PropertyFacetValues[pfv.Key] = pfv.Value;
            }
            return this;
        }
    }
}
