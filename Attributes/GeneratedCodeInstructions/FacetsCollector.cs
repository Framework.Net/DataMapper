﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using TechnicalTools;


namespace DataMapper
{
    public class FacetsCollector
    {
        public virtual AssemblyFacets CollectAndAggregateFacetRealUsages(Assembly assembly)
        {
            Debug.Assert(assembly != null);
            StaticInvariant();

            var assFacets = CreateAssemblyFacets();
            assFacets.Assembly = assembly;

            Type[] types;
            try
            {
                types = assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                types = e.Types.NotNull().ToArray();
            }
            assFacets.Classes = types.Select((t,i) => new { DeclaringClassType = t, Mapping = t.GetCustomAttributes<DbMappedTable>().SingleOrDefault() })
                                     .Where(t => t.Mapping != null)
                                     .Select(t =>
                                     {
                                         var cf = assFacets.CreateNewClassFacets(assFacets);
                                         cf.DeclaringClassType = t.DeclaringClassType;
                                         cf.Mapping = t.Mapping;
                                         return cf;
                                     })
                                     .ToList();
            foreach (var cf in assFacets.Classes)
            {
                var pi = cf.DeclaringClassType.GetProperty(ClassFilePathFieldName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                cf.RelativePath = (string)pi?.GetValue(null);

                cf.Properties = cf.DeclaringClassType
                                        .GetProperties()
                                        .Select(p =>
                                        {
                                            var pc = cf.CreateNewPropertyFacets(cf);
                                            pc.Property = p;
                                            pc.Mapping = p.GetCustomAttribute<DbMappedField>(true);
                                            if (p.PropertyType.RemoveNullability().IsEnum ||
                                                p.PropertyType.IsClass && !p.PropertyType.IsMicrosoftType())
                                                pc.ProjectedType = p.PropertyType;
                                            else
                                            {
                                                // We don't store other type so if they change in database generated code will be updated
                                            }
                                            if (!p.GetMethod.IsPublic || p.GetSetMethod() == null)
                                                pc.Accessibility = System.CodeDom.MemberAttributes.Private;
                                            return pc;
                                        })
                                        // Keep only mapped properties
                                        .Where(pf => pf.Mapping != null)
                                        .ToList();
            }
            return assFacets;
        }

        public static readonly string ClassFilePathFieldName = "ClassFilePath";

        public virtual AssemblyFacets CollectAndAggregateFacetDeclarations(Assembly assembly)
        {
            Debug.Assert(assembly != null);
            StaticInvariant();

            var assFacets = CreateAssemblyFacets();
            assFacets.Assembly = assembly;

            foreach (var gaa in assembly.GetCustomAttributes<GeneratedAssemblyAttribute>())
                assFacets.MergeWith(gaa, forbidOverwrite: true);


            var allClassFacetsFromAssembly = assembly.GetTypes()
                                                        .Select(t => new { t, Attributes = t.GetCustomAttributes<GeneratedClassAttribute>(true) })
                                                        .Where(t => t.Attributes.Any())
                                                        .ToDictionary(t => t.t, t => t.Attributes);
            assFacets.Classes = assembly.GetTypes()
                                        // Prefilter on class which are mappable (from IDbMapper perspective)
                                        .Where(t => typeof(IAutoMappedDbObject).IsAssignableFrom(t))
                                        .Select(t =>
                                        {
                                            var fc = assFacets.CreateNewClassFacets(assFacets);
                                            fc.DeclaringClassType = t;
                                            fc.Mapping = t.GetCustomAttribute<DbMappedTable>(true);
                                            return fc;
                                        })
                                        // Filter on class which are actually mapped (should just check that attribute exists, but there is still some corner cases/hacks today)
                                        .Where(cf => !string.IsNullOrWhiteSpace(cf.Mapping?.FullName))
                                        .Select(cf =>
                                        {
                                            // Merge and check all settings defined for this class (GetCustomAttributes is plural form because classes can be "partial")
                                            var fromAssembly = allClassFacetsFromAssembly.TryGetValueClass(cf.DeclaringClassType)
                                                            ?? Enumerable.Empty<GeneratedClassAttribute>();
                                            allClassFacetsFromAssembly.Remove(cf.DeclaringClassType);
                                            foreach (var otherCf in fromAssembly)
                                                cf.MergeWith(otherCf, forbidOverwrite: true);
                                            return cf;
                                        })
                                        .ToList();
            if (allClassFacetsFromAssembly.Any())
                throw new Exception($"Some attribute {nameof(GeneratedClassAttribute)} define in assembly are useless: " + Environment.NewLine +
                                    allClassFacetsFromAssembly.Select(kvp => " - on class " + kvp.Key.Name).Join(Environment.NewLine));


            var allPropertyFacetsFromClass = assembly.GetTypes()
                .ToDictionary(t => t, t =>
                {
                    // TODO : get all instances of GeneratedPropertyAttribute declared on ALL properties to detect useless ones.
                    var classAttributes = t.GetCustomAttributes<GeneratedPropertyAttribute>(true)
                                            .GroupBy(a => a.ForProperty?.Trim() ?? string.Empty);
                    if (classAttributes.Any(grp => grp.Key == string.Empty))
                        throw new Exception($"An attribute of type {typeof(GeneratedPropertyAttribute).Name} is declared on class {t.Name} and has not defined on which property it applies (Please fill property {nameof(GeneratedPropertyAttribute.For)}!");
                    return classAttributes.ToDictionary(grp => grp.Key);
                });
            foreach (var cf in assFacets.Classes)
            {
                cf.Properties = cf.DeclaringClassType
                                        .GetProperties()
                                        .Select(p =>
                                        {
                                            var pc = cf.CreateNewPropertyFacets(cf);
                                            pc.Property = p;
                                            pc.Mapping = p.GetCustomAttribute<DbMappedField>(true);
                                            return pc;
                                        })
                                        // Filter on class which are actually mapped
                                        .Where(pf => pf.Mapping != null)
                                        .ToList();
                foreach (var pf in cf.Properties)
                {
                    // Merge and check all settings defined for this property
                    var fromClass = allPropertyFacetsFromClass[cf.DeclaringClassType].TryGetValueClass(pf.Property.Name)
                                    ?? Enumerable.Empty<GeneratedPropertyAttribute>();
                    allPropertyFacetsFromClass[cf.DeclaringClassType].Remove(pf.Property.Name);
                    // attribute has allowmultiple = false, but just to concat
                    foreach (var otherPf in pf.Property.GetCustomAttributes<GeneratedPropertyAttribute>(true).Concat(fromClass))
                        pf.MergeWith(otherPf, forbidOverwrite: true);
                };
            }
            var uselessPropertyFacetsFromClass = allPropertyFacetsFromClass.SelectMany(tp => tp.Value.Select(p => new { Type = tp.Key, Property = p.Key, Declarations = p.Value })).ToList();
            // TODO : Not accurate
            //if (uselessPropertyFacetsFromClass.Any())
            //{
            //    var msg = $"Some attribute {nameof(GeneratedPropertyAttribute)} are useless: " + Environment.NewLine +
            //        uselessPropertyFacetsFromClass.Select(tpd => " - on class " + tpd.Type.Name + " for property/ies: " + tpd.Property)
            //                                        .Join(Environment.NewLine);
            //    throw new Exception(msg);
            //}
            return assFacets;
        }
        protected virtual AssemblyFacets CreateAssemblyFacets() { return new AssemblyFacets(); }

        static void StaticInvariant()
        {
            // Keep only properties that could be exposed facet
            bool keepPotentialFacetProperty(PropertyInfo p)
            {
                return (p.PropertyType.RemoveNullability().IsValueType ||
                        p.PropertyType == typeof(string) ||
                        p.PropertyType == typeof(Type))
                    && p.GetSetMethod(true).IsPublic;
            }

            var atts = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            // Check no potential ambiguity of setting from one outer scope to one inner scope
            var AssemblyFacetsProperties = typeof(AssemblyFacets).GetProperties(atts).Where(keepPotentialFacetProperty).ToDictionary(p => p.Name);
            var ClassFacetsProperties = typeof(ClassFacets).GetProperties(atts).Where(keepPotentialFacetProperty).ToDictionary(p => p.Name);
            var PropertyFacetsProperties = typeof(PropertyFacets).GetProperties(atts).Where(keepPotentialFacetProperty).ToDictionary(p => p.Name);
            var ambiguous = AssemblyFacetsProperties.Keys.Intersect(ClassFacetsProperties.Keys).ToList();
            Debug.Assert(!ambiguous.Any());
            ambiguous = AssemblyFacetsProperties.Keys.Intersect(PropertyFacetsProperties.Keys).ToList();
            Debug.Assert(!ambiguous.Any());
            ambiguous = ClassFacetsProperties.Keys.Intersect(PropertyFacetsProperties.Keys).ToList();
            Debug.Assert(!ambiguous.Any());
        }
    }
}
