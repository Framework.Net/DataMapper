﻿using System;

using DataMapper.Diagnostics;


namespace DataMapper
{
    public class IDbMapperSharedSettings
    {
        public static IDbMapperSharedSettings Instance { get; set; } = new IDbMapperSharedSettings();

        public event Action<RequestTrace> Request;

        public uint? BulkInsertTimeout { get; set; }

        /// <summary>
        /// Some sql requests may be done using streaming,
        /// that allows business code to enumerate items and begin to process them.
        /// Because when we want to do performance measurement we want to measure only network and ORM speed,
        /// this options force the full load of result set in memory before it is pass to business layer.
        /// </summary>
        public bool DisableStreaming { get; set; }

        public bool RequestEventHasSubscribers { get { return Request != null; } }

        internal void RaiseRequest(RequestTrace rt)
        {
            Request?.Invoke(rt);
        }
    }
}
