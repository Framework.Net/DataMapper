﻿using System;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace DataMapper
{
    // Fonctionalités offertes par les classes respectant l'interface IAutoMappedDbObject
    internal partial class DbMapperConcrete
    {
        public void AutoLoadItem<T>(T item, IIdTuple id)
           where T : IPropertyNotifierObject, IAutoMappedDbObject
        {
            item.CheckIsNotReadOnlyForAction(nameof(AutoLoadItem));
            item.LockWhileUpdatingWith(() => AutoLoadItem(item, id));
            item.UpdateState(eState.Synchronized);
        }

        public void CancelChanges<T>(T item)
              where T : IPropertyNotifierObject, IAutoMappedDbObject, IHasClosedIdReadable
        {
            item.CheckIsNotReadOnlyForAction(nameof(CancelChanges));
            if (item.State == eState.New)
            {
                var type = item.GetType();
                var cons = type.GetDefaultConstructor();
                Debug.Assert(cons != null, "Garanti par GetTableMapping");
                var fresh_object_of_same_type = cons.Invoke(new object[0]);

                foreach (var field in GetTableMapping(item).AllFields)
                    field.SetValue(item, field.GetValue(fresh_object_of_same_type));
                item.InitializeState(eState.New);
            }
            else if (item.State == eState.Unsynchronized)
            {
                Debug.Assert(!item.IsReadOnly, "L'objet est marqué readonly mais à quand même été modifié (Unsynchronized)." +
                                              "On ne peut pas recharger l'objet ! Il faut que celui qui a modifié l'objet repasse l'objet en IsReadOnly = false," +
                                              "appelle CancelChange puis repasse isReadONly à true");
                AutoLoadItem(item, item.Id);
                Debug.Assert(item.State == eState.Synchronized);
            }
        }

        public void SynchronizeToDatabase<TObject>(TObject item)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject
        {
            item.CheckIsNotReadOnlyForAction("SynchronizeToDatabase");

            switch (item.State)
            {
                case eState.Unsynchronized:
                    UpdateToDatabase(item, true);
                    break;

                case eState.New:
                    CreateInDatabase(item, true);
                    break;

                case eState.ToDelete:
                    DeleteInDatabase(item, true);
                    break;

                case eState.Deleted:
                    DebugTools.Should("Probleme potentiel détécté : Pourquoi resynchroniser un objet deja supprimé en base ?!");
                    break;
            }

            item.UpdateState();
        }


        #region Sucre syntaxique

        public void Save<T>(T item)
              where T : IPropertyNotifierObject, IAutoMappedDbObject
        {
            item.CheckIsNotReadOnlyForAction(nameof(Save));
            Debug.Assert(item.State.In(eState.New, eState.Unsynchronized, eState.Synchronized));
            SynchronizeToDatabase(item);
        }

        public void Delete<T>(T item)
              where T : IPropertyNotifierObject, IAutoMappedDbObject
        {
            item.CheckIsNotReadOnlyForAction(nameof(Delete));
            item.UpdateState(eState.ToDelete);
            if (item.State == eState.Deleted) // A été géré par quelqu'un d'autre ?!
                return;
            Debug.Assert(item.State == eState.ToDelete);
            SynchronizeToDatabase(item);
        }

        #endregion Sucre syntaxique

    }
}
