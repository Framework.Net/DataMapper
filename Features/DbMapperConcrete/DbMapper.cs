using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;

using DataMapper.Linq2Sql;


namespace DataMapper
{
    internal partial class DbMapperConcrete : IDbMapper
    {
        public IIsNamed Owner          { get; private set; }
        public bool     AsDisconnected { get; }
        public string GetConnectionString() { return GetConnectionProvider().ConnectionString; }
        public IReadOnlyDictionary<Type, Type> Mapping { get; }

        public DbMapperConcrete(IIsNamed owner, Func<IConnectionProvider> getcn, bool asDisconnected = false, IReadOnlyDictionary<Type, Type> mapping = null, SqlQueryProvider provider = null)
        {
            Owner = owner;
            AsDisconnected = asDisconnected;
            GetConnectionProvider = getcn;
            Mapping = mapping;
            InitLinq2SqlProvider(provider);
        }

        readonly MemoryCacheManager _cache = new MemoryCacheManager();

        internal Func<IConnectionProvider> GetConnectionProvider { get; private set; }

        public string GetTableNameFor<TObject>()
               where TObject : IAutoMappedDbObject
        {
            return GetTableMapping(typeof(TObject)).FullName;
        }
        [DebuggerHidden][DebuggerStepThrough]
        public string GetColumnNameFor<TObject>(string propertyName)
               where TObject : IAutoMappedDbObject
        {
            return GetTableMapping(typeof(TObject)).AllFieldsByPropertyName[propertyName]?.ColumnName;
        }
        [DebuggerHidden][DebuggerStepThrough]
        public string GetColumnNameFor<TObject, TProperty>(Expression<Func<TObject, TProperty>> getProperty)
               where TObject : IAutoMappedDbObject
        {
            return GetTableMapping(typeof(TObject)).AllFieldsByPropertyName[GetMemberName.For(getProperty)]?.ColumnName;
        }

        public TObject CreateInstanceOf<TObject>()
            where TObject : class, IAutoMappedDbObject
        {
            var mtable = GetTableMapping(typeof(TObject));
            return (TObject)DefaultObjectFactory.ConstructorFor(mtable.MappedType)();
        }


        public List<DbMappedTable> GetTableMappings()
        {
            return _mtables.Values.ToList();
        }
        readonly ConcurrentDictionary<Type, DbMappedTable> _mtables = new ConcurrentDictionary<Type, DbMappedTable>();
        public DbMappedTable GetTableMapping(Type type, bool buildIfNeeded = true)
        {
            Type substitute = null;
            if (Mapping?.TryGetValue(type, out substitute) ?? false)
                type = substitute;
            if (_mtables.TryGetValue(type, out DbMappedTable res))
                return res;
            lock (_mtables)
            {
                if (_mtables.TryGetValue(type, out res))
                    return res;
                var mtables = CreateTableMapping(type, new Dictionary<Type, DbMappedTable>());
                bool success;
                foreach (var kvp in mtables)
                {
                    success = _mtables.TryAdd(kvp.Key, kvp.Value);
                    Debug.Assert(success);
                }
                success = _mtables.TryGetValue(type, out res);
                Debug.Assert(success);
                return res;
            }
        }

        [Obsolete("Plus bon du tout!")]
        public IIdTuple GetSqlIds<TObject>(TObject obj)
            where TObject : IAutoMappedDbObject
        {
            DbMappedTable mtable = GetTableMapping(obj.GetType());
            if (mtable.PkFields.Count == 0) // table sans Id !
                return null;
            if (mtable.PkFields.Count == 1)
                return new IdTuple<int>((int)mtable.PkFields[0].GetDbValue(obj));

            if (mtable.PkFields.Count == 2 && mtable.PkFields[1].PropertyInfo.PropertyType == typeof(string))
                return new IdTuple<int, string>((int)mtable.PkFields[0].GetDbValue(obj), (string)mtable.PkFields[1].GetDbValue(obj));

            Debug.Assert(mtable.PkFields.All(f => f.GetDbValue(obj) is int), " pas encore géré ! Il faut complexifier le code qui suit pour gérer ce cas ");
            if (mtable.PkFields.Count == 2)
                return new IdTuple<int, int>((int)mtable.PkFields[0].GetDbValue(obj), (int)mtable.PkFields[1].GetDbValue(obj));
            if (mtable.PkFields.Count == 3)
                return new IdTuple<int, int, int>((int)mtable.PkFields[0].GetDbValue(obj), (int)mtable.PkFields[1].GetDbValue(obj), (int)mtable.PkFields[2].GetDbValue(obj));
            if (mtable.PkFields.Count == 4)
                return new IdTuple<int, int, int, int>((int)mtable.PkFields[0].GetDbValue(obj), (int)mtable.PkFields[1].GetDbValue(obj), (int)mtable.PkFields[2].GetDbValue(obj), (int)mtable.PkFields[3].GetDbValue(obj));

            throw new NotImplementedException("Il faut copier coller la classe IdTuple en ajoutant un parametre générique " + Environment.NewLine +
                                              "puis chercher les references à IdTuple<int, int, int, int> pour savoir où il faut ajouter du code pour prnedre en compte la nouvelle classe !");
        }

        public void SetSqlIds<TObject>(TObject obj, IIdTuple ids)
            where TObject : IAutoMappedDbObject
        {
            Debug.Assert(ids != null);
            DbMappedTable mtable = GetTableMapping(obj.GetType());
            if (mtable.PkFields.Count == 0)
                throw new InvalidOperationException($"The mapping of \"{typeof(TObject).FullName}\" does not define PK column(s)!");
            Debug.Assert(ids.Keys.Length == mtable.PkFields.Count);
            int i = 0;
            foreach (var field in mtable.PkFields)
            {
                var id = ids.Keys[i];
                Debug.Assert(id == null && (!field.PropertyInfo.PropertyType.IsValueType || field.PropertyInfo.PropertyType.IsNullable()) ||
                             id != null && id.GetType() == field.PropertyInfo.PropertyType, "Type mismatch !");
                field.SetDbValue(obj, id); // TODO : Ca genere mtable.PkFields.Count event Idchanged... peut mieux faire ?
                ++i;
            }
        }


        public void ChangeTableMapping<TObject>(string newTableName, string newSchemaName = "dbo")
           where TObject : IAutoMappedDbObject
        {
            ChangeTableMapping(typeof(TObject), newTableName, newSchemaName);
        }
        public void ChangeTableMapping(Type type, string newTableName, string newSchemaName = "dbo")
        {
            var newTableMapping = new DbMappedTable(newTableName, newSchemaName);
            _tableMappingOverrides.AddOrUpdate(type, newTableMapping, (_, __) => newTableMapping);
            _mtables.TryRemove(type, out newTableMapping);
        }
        readonly ConcurrentDictionary<Type, DbMappedTable> _tableMappingOverrides = new ConcurrentDictionary<Type, DbMappedTable>();

        Dictionary<Type, DbMappedTable> CreateTableMapping(Type type, Dictionary<Type, DbMappedTable> buildingTables)
        {
            Debug.Assert(type != null);
            Debug.Assert(typeof(IAutoMappedDbObject).IsAssignableFrom(type));

            var mtable = GetMappingOverride(type, GetDbMappedTableAttribute(type));
            mtable.MappedType = type;
            mtable.Mapper = this;
            if (mtable == null)
                throw new TechnicalException($"Le type {type.FullName} ou une de ses classes ancêtres, doit avoir un des attributs {typeof(DbMappedTable).Name} ou {typeof(DbMappedView).Name} !", null);

            buildingTables.Add(type, mtable);

            #region Build Column mappings

            PropertyInfo[] property_infos = type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.FlattenHierarchy);
            foreach (PropertyInfo prop_info in property_infos)
            {
                var attributes = prop_info.GetCustomAttributes(typeof(DbMappedPropertyAttribute), true).Cast<DbMappedPropertyAttribute>().ToArray();
                if (attributes.Length == 0)
                    continue;

                AnalyseProperty(type, prop_info, mtable, attributes, buildingTables);
            }

            var new_order = mtable.AllFields
                                 .OrderBy(f => f.PropertyInfo.DeclaringType.GetLevelOfHierarchy())
                                 .ThenBy(f => f.DeclaredAtLine).ToList();
            mtable.AllFields.Clear();
            mtable.AllFields.AddRange(new_order);
            for (int i = 0; i < mtable.AllFields.Count; ++i)
                mtable.AllFields[i].OrdinalIndex = i;
            foreach (var f in mtable.AllFields)
                mtable.AllFieldsByProperty.Add(f.PropertyInfo, f);
            foreach (var f in mtable.AllFields)
                mtable.AllFieldsByPropertyName.Add(f.PropertyInfo.Name, f);
            foreach (var f in mtable.AllFields)
                mtable.AllFieldsByColumnName.Add(f.ColumnName, f);

            if (mtable.PkFields.Count == 1 && mtable.PkFields[0].KeyOrderIndex == -1)
                mtable.PkFields[0].KeyOrderIndex = 1;

            new_order = mtable.PkFields.OrderBy(f => f.KeyOrderIndex).ToList();
            mtable.PkFields.Clear();
            mtable.PkFields.AddRange(new_order);


            var foreignKeys = type.GetCustomAttributes<DbForeignKey>(true);
            foreach (var foreignKey in foreignKeys)
                mtable.ForeignKeys.Add(foreignKey); // add explicit foreign key
            foreach (var fk in mtable.ForeignKeys) // browse all foreign key (not only explicit)
            {
                fk.Mapper = this;
                fk.Table = mtable;

                fk.Keys.Clear();
                foreach (var kpn in fk.KeyPropertiesNames)
                    if (fk.Table.AllFieldsByPropertyName.ContainsKey(kpn))
                        fk.Keys.Add(fk.Table.AllFieldsByPropertyName[kpn]);
                    else
                        throw new Exception($"Property {kpn} of type {fk.Table.MappedType} is not mapped to a column and cannot be used a foreign key declaration!");
                fk.KeyPropertiesNames = null;
            }
            foreach (var grpFks in mtable.ForeignKeys.GroupBy(fk => Tuple.Create(fk.RelationName, fk.ConditionalPropertyName)))
            {
                DbForeignKey previous = null;
                foreach (var fk in grpFks)
                {
                    if (previous != null)
                        previous.Next = fk;
                    fk.Previous = previous;
                    previous = fk;
                }
            }

            #endregion

            #region Hard Checks

            var invalidPkFields = mtable.PkFields.Where(f => f.KeyOrderIndex < 1 || f.KeyOrderIndex > mtable.PkFields.Count)
                                        .Select(f => " - " + f.PropertyInfo.Name)
                                        .Join(Environment.NewLine);
            if (!string.IsNullOrWhiteSpace(invalidPkFields))
                throw new Exception($"Class {mtable.MappedType.FullName} has properties composing the primary keys that have invalid value for {nameof(DbMappedField.KeyOrderIndex)}" + Environment.NewLine + invalidPkFields);
            var indexAreContiguous = mtable.PkFields.Select(f => f.KeyOrderIndex).Distinct().Count() == mtable.PkFields.Count;
            if (!indexAreContiguous)
                throw new Exception($"Class {mtable.MappedType.FullName} has properties composing the primary keys that have not contiguous value for {nameof(DbMappedField.KeyOrderIndex)}");

            foreach (var fk in mtable.ForeignKeys)
            {
                var foreignTable = GetTableMappingOrContinueBuilding(fk.To, buildingTables);
                if (foreignTable.PkFields.Count == 0)
                    throw new Exception($"Foreign Key defined on {type.Name}.{fk.RelationName ?? fk.Keys.First().PropertyInfo.Name} is invalid because there is no PK defined in code on type {fk.To.Name}'!");
                var foreignPkType = foreignTable.PkFields.FirstOrDefault()?.PropertyInfo?.PropertyType;
                if (fk.Keys.Count == 1)
                    if (typeof(DynamicEnum).IsAssignableFrom(fk.Keys[0].PropertyInfo.PropertyType) &&
                        GetTableMappingOrContinueBuilding(fk.Keys[0].PropertyInfo.PropertyType, buildingTables).PkFields.Count == 1 &&
                        GetTableMappingOrContinueBuilding(fk.Keys[0].PropertyInfo.PropertyType, buildingTables).PkField.PropertyInfo.PropertyType == foreignPkType)
                        continue;
                    else if (!fk.Keys[0].IsNullable && foreignPkType != fk.Keys[0].PropertyInfo.PropertyType)
                        throw new Exception($"Foreign Key defined on {type.Name}.{fk.Keys[0].PropertyInfo.Name} is invalid because type {fk.To.Name}' PK is composed of multiple columns!");
                    // doit gerer le nullable<> pour les value type, et les string implictement nullable
                    else if (fk.Keys[0].IsNullable && foreignPkType != fk.Keys[0].PropertyInfo.PropertyType.RemoveNullability())
                        throw new Exception($"La propriété {type.Name}.{fk.Keys[0].PropertyInfo.Name} marquée en tant que foreign key vers le type {fk.To.Name} doit matcher le type de sa PK ({foreignPkType}) !");
                // Check for fk.Keys.Count > 1 to do!
            }
            #endregion

            CheckMappingWeirdness(mtable);

            AnalyseAdvancedRelationships(type, buildingTables, mtable);

            //mtable.KeepInCache = type.GetCustomAttribute<KeepInCacheAttribute>(true);

            return buildingTables;
        }

        DbMappedTable GetDbMappedTableAttribute(Type type)
        {
            DbMappedTable[] mappings = null;
            Type t = type;
            while (((mappings = (DbMappedTable[])t.GetCustomAttributes(typeof(DbMappedTable), false))?.Length ?? 0) == 0)
            {
                t = t.BaseType;
                if (t == null)
                    break;
            }
            if (mappings.Length > 1)
                throw new Exception($"La classe {t.FullName} ne peut avoir deux attributs {typeof(DbMappedTable).Name} ou {typeof(DbMappedView).Name} en même temps.");
            var mtable = mappings.FirstOrDefault();
            if (mtable == null)
                return mtable;
            return mtable;
        }

        DbMappedTable GetMappingOverride(Type type, DbMappedTable mtable)
        {
            if (_tableMappingOverrides.TryGetValue(type, out DbMappedTable overridingTableMapping))
            {
                if (mtable == null)
                    return overridingTableMapping; // TODO : make a full copy ?
                mtable.SchemaName = overridingTableMapping.SchemaName;
                mtable.TableName = overridingTableMapping.TableName;
            }
            return mtable;
        }

        void AnalyseProperty(Type analysedType, PropertyInfo prop_info, DbMappedTable mtable, DbMappedPropertyAttribute[] attributes, Dictionary<Type, DbMappedTable> buildingTables)
        {
            attributes[0].Mapper = this;
            attributes[0].Table = mtable;
            attributes[0].PropertyInfo = prop_info;

            if (attributes[0] is DbAggregation agg_mapping)
                mtable.Aggregations.Add(agg_mapping);
            if (attributes[0] is DbAggregationReverse revagg_mapping)
                mtable.ReverseAggregations.Add(revagg_mapping);
            if (attributes[0] is DbComposition comp_mapping)
                mtable.Compositions.Add(comp_mapping);
            if (attributes[0] is DbCompositionReverse revcomp_mapping)
                if (mtable.CompositionOwner != null)
                    throw new TechnicalException("Only one composition Owner can exist!", null);
                else
                    mtable.CompositionOwner = revcomp_mapping;

            if (attributes[0] is DbMappedField field_mapping)
            {
                Debug.Assert(prop_info.GetGetMethod(true) != null, $"La propriété {prop_info.Name} de la classe {analysedType.Name} doit avoir un accesseur en lecture pour que l'attribut {field_mapping.GetType().Name} fonctionne !");
                Debug.Assert(prop_info.GetSetMethod(true) != null, $"La propriété {prop_info.Name} de la classe {analysedType.Name} doit avoir un accesseur en écriture pour que l'attribut {field_mapping.GetType().Name} fonctionne !");

                mtable.AllFields.Add(field_mapping);
                if (field_mapping.IsPK)
                    mtable.PkFields.Add(field_mapping);
                else
                    mtable.NonPKFields.Add(field_mapping);
                if (field_mapping.OldFkTo != null)
                {
                    // use constructor the user should use
                    mtable.ForeignKeys.Add(new DbForeignKey(field_mapping.OldFkTo, field_mapping.PropertyInfo.Name));
                }

                // On teste que la valeur par defaut est du bon type
                if (field_mapping.HasDefaultValue)
                    mtable.FieldsWithDefaultValues.Add(field_mapping);

                Type prop_type = prop_info.PropertyType;
                if (prop_type == typeof(string))
                    SetConversionForNonNullableString(field_mapping);
                else if (prop_type == typeof(byte[]))
                    SetConversionForNonNullableByteArray(field_mapping);
                else if (prop_type.IsValueType || (prop_type.TryGetNullableType() ?? prop_type).IsEnum)
                    SetConversionForNonNullableValueType(field_mapping);
                else if (typeof(DynamicEnum).IsAssignableFrom(prop_type))
                    SetConversionForNonNullableDynamicEnum(analysedType, prop_info, mtable, buildingTables, field_mapping, prop_type);
                else if (typeof(IStaticEnum).IsAssignableFrom(prop_type))
                    SetConversionForNonNullableValueTypeAsStaticEnum(mtable, field_mapping, prop_type);
                else if (typeof(Version) == (prop_type.TryGetNullableType() ?? prop_type))
                    SetConversionForNonNullableVersion(field_mapping);
                //else if (typeof(SqlXml) == prop_type)
                //    SetConversionForSqlXml(field_mapping);
                else
                    throw new Exception($"Le type SQL {prop_type.Name} n'est pas géré pour le moment." + Environment.NewLine +
                                        "Seules les propriétés de type Nullable(of ...), string, entier ou enum sont gérées");

                AdaptConversionForNullableValue(field_mapping);
            }
        }

        static void SetConversionForNonNullableString(DbMappedField field_mapping)
        {
            field_mapping.ConvertDbToCsValue = value =>
            {
                if (field_mapping.Encoding == eEncoding.Iso_8859_1)
                {
                    var from = Encoding.GetEncoding("iso-8859-1");
                    var to = Encoding.UTF8; //= Encoding.GetEncoding(1252);
                    byte[] isoBytes = from.GetBytes((string)value);
                    byte[] unicBytes = Encoding.Convert(from, to, isoBytes); // necessaire pour mb_prod_collecticity.te_cash_exchange_new(id = 234).comment
                    var result = to.GetString(unicBytes);
                    return result;
                }
                else if (field_mapping.Encoding != eEncoding.Unicode)
                    throw new TechnicalException("Encoding " + field_mapping.Encoding + " not handled yet !", null);
                return value;
            };
            // Nothing to do to manage encoding because we send request as N'...'
            field_mapping.ConvertCsToDbValue = Func_Extensions<object>.Identity;
        }

        static void SetConversionForNonNullableByteArray(DbMappedField field_mapping)
        {
            field_mapping.ConvertDbToCsValue = Func_Extensions<object>.Identity;
            field_mapping.ConvertCsToDbValue = Func_Extensions<object>.Identity;
        }

        static void SetConversionForNonNullableValueType(DbMappedField field_mapping)
        {
            field_mapping.ConvertDbToCsValue = Func_Extensions<object>.Identity;
            field_mapping.ConvertCsToDbValue = Func_Extensions<object>.Identity;
        }

        void SetConversionForNonNullableDynamicEnum(Type analysedType, PropertyInfo prop_info, DbMappedTable mtable, Dictionary<Type, DbMappedTable> buildingTables, DbMappedField field_mapping, Type prop_type)
        {
            var mTableForeign = GetTableMappingOrContinueBuilding(prop_type, buildingTables);
            if (mTableForeign.PkFields.Count != 1 ||
                (!mTableForeign.PkField.PropertyInfo.PropertyType.IsPrimitive &&
                mTableForeign.PkField.PropertyInfo.PropertyType != typeof(string) &&
                mTableForeign.PkField.PropertyInfo.PropertyType != typeof(DateTime) &&
                mTableForeign.PkField.PropertyInfo.PropertyType != typeof(TimeSpan) &&
                mTableForeign.PkField.PropertyInfo.PropertyType != typeof(Version)))
                throw new TechnicalException("Graph model not handled: " + analysedType.Name + " type reference (via property " + prop_info.Name + ") the type " + prop_type.Name + "." + Environment.NewLine +
                                             "But this latter has for PK the type " + mTableForeign.PkField.PropertyInfo.PropertyType.Name + " which is not a native C# type", null);

            var mGetValueFor = prop_type.GetMethod(nameof(DynamicEnum.DummyEnum._GetValueFor), BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            Debug.Assert(mGetValueFor != null);
            field_mapping.ConvertDbToCsValue = id => mGetValueFor.Invoke(null, new[] { Convert.ChangeType(id, mGetValueFor.GetParameters().First().ParameterType), null });
            field_mapping.ConvertCsToDbValue = dynEnumValue => Convert.ChangeType(((DynamicEnum)dynEnumValue).EnumId, mTableForeign.PkField.PropertyInfo.PropertyType);

            if (field_mapping.OldFkTo == null)
                field_mapping.OldFkTo = prop_type;
        }

        static void SetConversionForNonNullableValueTypeAsStaticEnum(DbMappedTable mtable, DbMappedField field_mapping, Type prop_type)
        {
            var dic = IStaticEnum_Extensions.GetValueSet(prop_type);
            field_mapping.ConvertDbToCsValue = id => dic[id.ToStringInvariant()];
            field_mapping.ConvertCsToDbValue = staticEnumValue => int.Parse(((IStaticEnum)staticEnumValue).Code);

            if (field_mapping.OldFkTo == null)
                field_mapping.OldFkTo = prop_type;
        }

        static void SetConversionForNonNullableVersion(DbMappedField field_mapping)
        {
            field_mapping.ConvertDbToCsValue = str => Version.Parse((string)str);
            field_mapping.ConvertCsToDbValue = v => v.ToString();
        }

        //static void SetConversionForSqlXml(DbMappedField field_mapping)
        //{
        //    // Datamapper seems to map sql type "Xml(.)" to string currently
        //    // Hesitating between this mapping
        //    field_mapping.ConvertDbToCsValue = v => v;
        //    field_mapping.ConvertCsToDbValue = v => v;
        //    // or this one (but prop_type would need to be of type string)
        //    field_mapping.ConvertDbToCsValue = v => ((SqlXml)v).IsNull ? null : ((SqlXml)v).Value;
        //    // Dispose methods of these stream do nothing actually,
        //    // so SqlXml instance can keep the reader...
        //    field_mapping.ConvertCsToDbValue = v => new SqlXml(v == null ? null : new XmlTextReader(new StringReader((string)v)));
        //}

        static void AdaptConversionForNullableValue(DbMappedField field_mapping)
        {
            if (!field_mapping.IsNullable)
                return;
            var dbtocs = field_mapping.ConvertDbToCsValue;
            field_mapping.ConvertDbToCsValue = value =>
            {
                if (ReferenceEquals(value, DBNull.Value))
                    return null;
                return dbtocs(value);
            };
            var cstodb = field_mapping.ConvertCsToDbValue;
            field_mapping.ConvertCsToDbValue = value =>
            {
                if (value == null)
                    return DBNull.Value;
                return cstodb(value);
            };
        }

        void CheckMappingWeirdness(DbMappedTable mtable)
        {
            string doublon_report = mtable.AllFields.FindDuplicatesAndMakeReport(f => f.ColumnName, f => f.PropertyInfo.Name + " est mappé sur " + f.ColumnName);
            Debug.Assert(string.IsNullOrEmpty(doublon_report), "Attention ! Plusieurs propriétés sont mappées sur la même colonne en base ! A savoir : " + Environment.NewLine + doublon_report);

            // Some checks
            if (mtable is DbMappedJoinTable)
                Debug.Assert(mtable.PkFields.Count > 1, $"Please use attribute {typeof(DbMappedTable).Name} or {typeof(DbMappedView).Name} instead of attribute {mtable.GetType().FullName} on class {mtable.MappedType.Name}, " +
                                                        "because there is no or only one property/column marked as primary key!");
            else
            {
                bool thereAreMultipleFkInPk = mtable.ForeignKeys
                                                    // fk is fully contained in PK
                                                    .Where(fk => fk.Keys.All(fkf => mtable.PkFields.Contains(fkf)))
                                                    // group similar "dynamic" FK (see DbForeignKey.Conditional* properties)
                                                    .GroupBy(fk => fk.RelationName)
                                                    // and there are at least two of them
                                                    .Count() > 1;
                bool thereIsAtLeastOneINterestingDataColumn = mtable.AllFields.Count > mtable.FkFields.Count();
                // If PK is composed of FK pointing to different tables and there is no additional info column, this is a DbMappedJoinTable
                Debug.Assert(!thereAreMultipleFkInPk || !thereIsAtLeastOneINterestingDataColumn,
                             $"Type {mtable.MappedType.FullName} mapped to {mtable.FullName} should use attribute" +
                             $" {nameof(DbMappedJoinTable)} instead of {nameof(DbMappedTable)}." + Environment.NewLine +
                             "Because:" + Environment.NewLine +
                             " - Multiple foreign keys compose the primary key" + Environment.NewLine +
                             " - There is no additional data column");
            }
        }
        void AnalyseAdvancedRelationships(Type type, Dictionary<Type, DbMappedTable> buildingTables, DbMappedTable mtable)
        {
            foreach (var comp_mapping in mtable.Compositions)
            {
                Debug.Assert(comp_mapping.FkPropertyName != null);
                if (comp_mapping.ReferencingClass == null)
                {
                    comp_mapping.ReferencingClass = type;
                    comp_mapping.ReferencingTable = mtable;
                }
                else
                    comp_mapping.ReferencingTable = GetTableMappingOrContinueBuilding(comp_mapping.ReferencingClass, buildingTables);

                comp_mapping.KeyField = comp_mapping.ReferencingTable.AllFieldsByPropertyName[comp_mapping.FkPropertyName];
                if (comp_mapping.KeyField.OldFkTo == null)
                    throw new Exception($"La propriété {comp_mapping.Table.MappedType.Name}.{comp_mapping.PropertyInfo.Name} de type {comp_mapping.PropertyInfo.Name}" +
                                        $" marquée en tant que composition vers le type {comp_mapping.PropertyInfo.PropertyType.Name} fait reference à la propriete {comp_mapping.KeyField.PropertyInfo.Name}. " + Environment.NewLine +
                                        $"Mais l'attribut de cette derniere ne possède pas de valeur pour {nameof(DbMappedField.FkTo)} !");
            }
            foreach (var agg_mapping in mtable.Aggregations)
            {
                Debug.Assert(agg_mapping.FkPropertyName != null);
                if (agg_mapping.ReferencingClass == null)
                {
                    agg_mapping.ReferencingClass = type;
                    agg_mapping.ReferencingTable = mtable;
                }
                else // TODO : prevent cycle ?!!
                    agg_mapping.ReferencingTable = GetTableMappingOrContinueBuilding(agg_mapping.ReferencingClass, buildingTables);

                agg_mapping.KeyField = agg_mapping.ReferencingTable.AllFieldsByPropertyName[agg_mapping.FkPropertyName];
                if (agg_mapping.KeyField.OldFkTo == null)
                    throw new Exception($"La propriété {agg_mapping.Table.MappedType.Name}.{agg_mapping.PropertyInfo.Name} de type {agg_mapping.PropertyInfo.Name}" +
                                        $" marquée en tant qu'aggregation vers le type {agg_mapping.PropertyInfo.PropertyType.Name} fait referecne a la propriete {agg_mapping.KeyField.PropertyInfo.Name}. " + Environment.NewLine +
                                        $"Mais l'attribut de cette derniere ne possède pas de valeur pour {nameof(DbMappedField.FkTo)} !");
            }

            if (mtable.CompositionOwner != null)
            {
                Debug.Assert(mtable.CompositionOwner.OwnerClass != null);
                Debug.Assert(mtable.CompositionOwner.CompositionPropertyName != null);
                var ownerTable = GetTableMappingOrContinueBuilding(mtable.CompositionOwner.OwnerClass, buildingTables);
                mtable.CompositionOwner.CompositionRelation = ownerTable.Compositions.First(a => a.PropertyInfo.Name == mtable.CompositionOwner.CompositionPropertyName);
            }
            foreach (var revagg_mapping in mtable.ReverseAggregations)
            {
                Debug.Assert(revagg_mapping.OwningClass != null);
                Debug.Assert(revagg_mapping.AggregationPropertyName != null);
                var ownerTable = GetTableMappingOrContinueBuilding(revagg_mapping.OwningClass, buildingTables);
                revagg_mapping.AggregationRelation = ownerTable.Aggregations.First(a => a.PropertyInfo.Name == revagg_mapping.AggregationPropertyName);
            }
        }

        DbMappedTable GetTableMappingOrContinueBuilding(Type type, Dictionary<Type, DbMappedTable> buildingTables)
        {
            if (Mapping != null &&
                Mapping.TryGetValue(type, out Type substitute))
                type = substitute;
            if (_mtables.TryGetValue(type, out var res))
                return res;
            if (buildingTables.TryGetValue(type, out res))
                return res;
            var mtables = CreateTableMapping(type, buildingTables);
            var success = mtables.TryGetValue(type, out res);
            Debug.Assert(success);
            return res;
        }
   }
}
