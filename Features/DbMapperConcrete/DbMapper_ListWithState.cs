﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;

namespace DataMapper
{
    internal partial class DbMapperConcrete
    {
        public List<TObject> AutoLoadCollection<TObject>(string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IPropertyNotifierObject, new()
        {
            return LoadCollection<TObject>(whereCondition);
        }
        [Obsolete("")]
        public void AutoLoadCollection<TObject>(CompositeList<TObject> collection, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IPropertyNotifierObject, new()
        {
            collection.CheckIsNotReadOnlyForAction("AutoLoadCollection");
            var lst = LoadCollection<TObject>(whereCondition);
            collection.Reset();
            foreach (var item in lst)
                collection.Add(item);
            collection.InitializeState(eState.Synchronized);
        }



        public void AutoLoadCollectionFromRows<TObject>(CompositeList<TObject> collection, IEnumerable<DataRow> rows)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new()
        {
            collection.CheckIsNotReadOnlyForAction("AutoLoadCollection");
            collection.Reset();
            AutoLoadCollection<TObject>(rows, collection.Add);
            collection.InitializeState(eState.Synchronized);
        }

        internal void AutoLoadCollection<TObject>(IEnumerable<DataRow> rows, Action<TObject> @add)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new()
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));

            foreach (DataRow dr in rows)
            {
                var item = new TObject();
                LoadProperties(item, dr, mtable);
                var notifier = item as IPropertyNotifierObject;
                //if (notifier != null)
                    notifier.UpdateState(eState.Synchronized);
                @add(item);
            }
        }



        public void AutoSaveCollection<TObject>(CompositeList<TObject> collection)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new()
        {
            collection.CheckIsNotReadOnlyForAction("AutoSaveCollection");
            if (collection.State == eState.Synchronized)
                return;
            Debug.Assert(collection.State.In(eState.New, eState.Unsynchronized), " Les autres cas sont non gérés pour le moment !");
            Debug.Assert(collection.RememberDeletedObjects, "Avant l'édition vous devez avoir mis RememberDeletedObjects sur true");

            AutoSaveItems(collection.Concat(collection.DeletedObjects.Where(item => item.State.NotIn(eState.New, eState.Deleted))));
            collection.UpdateState(eState.Synchronized);
        }

        public void AutoSaveItems<TObject>(IEnumerable<TObject> collection)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new()
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));

            var new_items_to_refresh = new List<TObject>();
            // Construit la requête avec tous les updates et delete à faire
            string sql_updates_and_deletes = string.Empty;
            foreach (TObject item in collection)
            {
                switch (item.State)
                {
                    case eState.Synchronized:
                        break; // Rien à faire

                    case eState.Deleted:
                        break; // Rien à faire

                    case eState.New:
                        new_items_to_refresh.Add(item);
                        break;

                    case eState.ToDelete:
                        sql_updates_and_deletes += GetSqlFor_DeleteInDatabase(item, mtable) + Environment.NewLine;
                        break;

                    case eState.Unsynchronized:
                        sql_updates_and_deletes += GetSqlFor_UpdateToDatabase(item, mtable) + Environment.NewLine;
                        break;
                }
            }

            if (string.IsNullOrWhiteSpace(sql_updates_and_deletes) & new_items_to_refresh.Count == 0)
            {
                // TODO : remettre en Debug.Assert un de ces 4
                DebugTools.Should("Incohérence trouvée : " + Environment.NewLine +
                                  "La collection est marquée comme modifiée mais tous ses éléments sont tous cleans !");
            }
            using (var cn = GetConnectionProvider())
                // ReSharper disable once AccessToDisposedClosure
                cn.WithConnectionDo((sqlConnection, itran) =>
            {
                try
                {
                    // En regle général les nouveaux objets se basent sur un modèle de donné à jour coté client.
                    // On fait donc d'abord les mise à jour des objets en base avant de créer les nouveaux objets en base qui
                    // potentiellement peuvent se baser sur les donne mise à jour (foreign key)
                    // L'inverse peut être vrai aussi mais dans ce cas on considèrera que l'écran qui a permis de creer un objet a enregistré
                    // les nouveaux objets avant de modifier les objets deja existant, les réferençant
                    if (!string.IsNullOrWhiteSpace(sql_updates_and_deletes))
                        cn.ExecuteNonQuery(sql_updates_and_deletes);

                        foreach (TObject item in new_items_to_refresh)
                            CreateInDatabase(item,  false);
                    itran.Commit();
                }
                catch
                {
                    DebugTools.Break();
                    itran.RollBack();
                    throw;
                }
            });
        }


        [Conditional("DEBUG")]
        public void CheckAllItemsAreDistinctByKey<TObject>(CompositeList<TObject> collection) where TObject : IAutoMappedDbObject, IPropertyNotifierObject
        {
            collection.CheckIsNotReadOnlyForAction("AutoSaveCollection");
            var mtable = GetTableMapping(typeof(TObject));
            var keys = collection.Select(item => mtable.PkFields.Select(f => (f.GetValue(item) ?? "").ToString()).Join("|")).ToList();
            Debug.Assert(keys.Count == keys.Distinct().Count(), "Doublons détectés");
        }

    }

}
