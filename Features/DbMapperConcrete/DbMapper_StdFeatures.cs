﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model.Cache;


namespace DataMapper
{
    internal partial class DbMapperConcrete
    {
        public DataTable GetDataTable(string query, RemoteContext context = null)
        {
            DataTable dt = null;
            using (var cn = GetConnectionProvider())
                Trace(cn, query, () =>
                {
                    dt = cn.GetDataTable(query, context);
                });
            return dt;
        }

        public decimal? GetDecimalScalar(string query)
        {
            var dt = GetDataTable(query);
            Debug.Assert(dt.Rows.Count <= 1);
            return dt.AsEnumerable().Select(row => row[0] is decimal ? (decimal)row[0]
                                                 : row[0] == DBNull.Value ? (decimal?)null
                                                 : row[0].GetType().IsNumericType() ? Convert.ToDecimal(row[0])
                                                 : (decimal)row[0])
                                    .FirstOrDefault(); // Let cast throws an exception...
        }

        public IEnumerable<TObject> ReadAndEnumerate<TObject>(string query, Func<DbDataReader, TObject> treat)
        {
            using (var cn = GetConnectionProvider())
                foreach (var reader in TraceEnumerable(cn, query, cn.GetEnumerableReader(query)))
                    yield return treat(reader);
        }


        public IEnumerable<DbMappedField> EnumerateEditedFields<TObject>(TObject x, TObject y)
            where TObject : IAutoMappedDbObject
        {
            return GetTableMapping(x).AllFields
                                     .Where(f => !Equals(f.GetValue(x), f.GetValue(y)))
                                     .ToList();
        }

        public void Attach<TObject>(TObject item)
            where TObject : BaseDTO, IAutoMappedDbObject, IHasTypedIdReadable
        {
            item.OwnerMapper = this;
        }
    }
}
