@echo off

echo.
echo  ** Loading git submodule (if any)
echo git submodule update --init --recursive
git submodule update --init --recursive

pause
