﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using TechnicalTools.Model;

using DataMapper.Linq2Sql;


namespace DataMapper
{
    public static class DbMapperFactory
    {
        public static IDbMapper CreateSqlMapper(IIsNamed owner, string connectionString, bool asDisconnected = false, IReadOnlyDictionary<Type, Type> mapping = null, SqlQueryProvider provider = null)
        {
            Debug.Assert(connectionString != null);
            return new DbMapperConcrete(owner, () => ConnectionProviderInstanciator(connectionString), asDisconnected, mapping, provider);
        }
        public static IDbMapper CreateSqlMapperWithLocalCache(IIsNamed owner, string connectionString, bool asDisconnected, string cacheLocalPath, Version softwareVersion)
        {
            Debug.Assert(connectionString != null);
            return new DbMapperWithLocalCache(CreateSqlMapper(owner, connectionString, asDisconnected), cacheLocalPath, softwareVersion);
        }
        public static IDbMapper CreateSqlMapperWithNotification(IIsNamed owner, string connectionString, bool asDisconnected)
        {
            Debug.Assert(connectionString != null);
            return new DbMapperWrapperNotifier(CreateSqlMapper(owner, connectionString, asDisconnected));
        }

        public static IDbMapper CreateSqlMapperWithAllFeatures(IIsNamed owner, string connectionString, bool asDisconnected, string cacheLocalPath, Version softwareVersion)
        {
            Debug.Assert(connectionString != null);
            return new DbMapperWrapperNotifier(new DbMapperWithLocalCache(CreateSqlMapper(owner, connectionString, asDisconnected), cacheLocalPath, softwareVersion));
        }

        public static Func<string, IConnectionProvider> ConnectionProviderInstanciator { get; set; } = connectionString => new SqlFeaturesProvider(connectionString);
    }
}
