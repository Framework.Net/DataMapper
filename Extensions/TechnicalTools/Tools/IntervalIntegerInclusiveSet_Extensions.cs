﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace DataMapper
{
    public class LongIdSet : IntervalIntegerInclusiveSet<LongIdSet, Interval>
    {
        public LongIdSet()                         : base()             { }
        public LongIdSet(long lower, long upper)   : base(lower, upper) { }
        public LongIdSet(IEnumerable<long> points) : base(points)       { }
        protected LongIdSet(IntervalIntegerInclusiveSet<LongIdSet, Interval> from) : base(from) { }

        protected override Interval       CreateNewInterval(long l, long u) { return new Interval(l, u); }
        protected override LongIdSet CreateNewSet()                                                          { return new LongIdSet(); }
        protected override LongIdSet CreateNewSet(IntervalIntegerInclusiveSet<LongIdSet, Interval> set) { return new LongIdSet((LongIdSet)set); }
    }

    public class Interval : TechnicalTools.Tools.IntervalIntegerInclusive, IAutoMappedDbObject
    {
        [DbMappedField("LowerBound", IsPK = true)] public new long LowerBound { get { return base.LowerBound; } protected set { base.LowerBound = value; } }
        [DbMappedField("UpperBound")]              public new long UpperBound { get { return base.UpperBound; } protected set { base.UpperBound = value; } }

        public Interval(long lowerbound, long upperbound)
            : base(lowerbound, upperbound)
        {
        }

        public override TechnicalTools.Tools.IntervalIntegerInclusive Clone()
        {
            return new Interval(LowerBound, UpperBound);
        }

        void CopyFrom(Interval source)
        {
            LowerBound = source.LowerBound;
            UpperBound = source.UpperBound;
        }
        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((Interval)source); }
        void ICopyable                     .CopyFrom(ICopyable           source) { CopyFrom((Interval)source); }
    }

    public static class IntervalIntegerInclusiveSet_Extensions
    {
        public static LongIdSet ToMappedIntegerSet<TItem>(this IEnumerable<TItem> source, Func<TItem, long> getValue)
        {
            return new LongIdSet(source.Select(getValue));
        }
        public static LongIdSet ToMappedIntegerSet(this IEnumerable<long> source)
        {
            return new LongIdSet(source);
        }

        public static string SplitFunction { get; set; }

        public static string AsSqlCondition(this LongIdSet set, string columnName, bool include = true)
        {
            if (!string.IsNullOrWhiteSpace(SplitFunction))
                return AsSqlCondition(set, columnName, SplitFunction, include: include);
            if (set.IsEmpty)
                return include ? "1 = 0" : "1 = 1";

            string notSpace = include ? "" : "    ";
            string multipleSpace = set.Intervals.Count > 1 ? "    " : "";
            string sqlCondition = set.Intervals.Where(interval => interval.LowerBound != interval.UpperBound)
                .Select(interval => columnName +
                                    " BETWEEN " + interval.LowerBound + " AND " + interval.UpperBound)
                .Join(Environment.NewLine + notSpace + " OR ");
            string uniqueIds = set.Intervals.Where(interval => interval.LowerBound == interval.UpperBound)
                .Select(interval => interval.LowerBound)
                .Join();
            if (sqlCondition.Length > 0 && multipleSpace.Length > 0)
                sqlCondition = multipleSpace.Substring(1) + sqlCondition;
            if (uniqueIds.Length > 0)
            {
                uniqueIds = columnName + " in (" + uniqueIds + ")";
                if (sqlCondition.Length > 0)
                    sqlCondition = sqlCondition + Environment.NewLine + notSpace + " OR " + uniqueIds;
                else
                    sqlCondition += uniqueIds;
            }

            if (set.Intervals.Count > 1)
                sqlCondition = "(" + sqlCondition + ")";
            if (!include)
                sqlCondition = "NOT " + sqlCondition;
            return sqlCondition;
        }

        /// <summary>
        /// The generated sql condition is much more efficient (~ factor 100 !!!) when we use this method
        /// than the one that does not have splitFunction argument
        /// </summary>
        public static string AsSqlCondition(this LongIdSet set, string columnName, string splitFunction, string tempTableName = "idSet", bool include = true)
        {
            if (set.IsEmpty)
                return include ? "1 = 0" : "1 = 1";

            var sb = new StringBuilder(include ? "" : "not ");
            sb.Append("exists (select top 1 1 from ");
            sb.Append(splitFunction);
            sb.Append("('");
            foreach (var i in set.Intervals)
            {
                sb.Append(i.LowerBound.ToStringInvariant());
                if (i.LowerBound != i.UpperBound)
                {
                    sb.Append('-');
                    sb.Append(i.UpperBound.ToStringInvariant());
                }

                sb.Append(',');
            }
            sb.Length = sb.Length - 1;
            sb.AppendLine("')");
            sb.Append("as ");
            sb.Append(tempTableName);
            sb.Append(" where ");
            sb.Append(columnName);
            sb.Append(" between ");
            sb.Append(tempTableName);
            sb.Append(".lower and ");
            sb.Append(tempTableName);
            sb.Append(".upper");
            sb.Append(")");
            return sb.ToString();
        }

        /// <summary>
        /// This method is like AsSqlCondition but use the string_split native function of SqlServer
        /// <remarks>As a side effect, just know that all the id in set will be sent one by one.
        /// while AsSqlCondition can send interval.</remarks>
        /// <para>
        /// AsSqlCondition     will send '1-1000000'
        /// </para>
        /// <para>
        /// AsSqlCondition2016 will send '1,2,3,4,5,etc...,1000000'
        /// </para>
        /// </summary>
        public static string AsSqlCondition2016(this LongIdSet set, string columnName, string tempTableName = "idSet", bool include = true)
        {
            if (!set.IsEmpty)
                throw new NotImplementedException("Not tested yet because no SQL server 2016 at disposal!");

            if (set.IsEmpty)
                return include ? "1 = 0" : "1 = 1";

            var sb = new StringBuilder(include ? "" : "not ");
            sb.Append("exists (select top 1 1 from ");
            sb.Append("string_split");
            sb.Append("('");
            foreach (var value in set.EnumerateAll())
            {
                sb.Append(value.ToStringInvariant());
                sb.Append(',');
            }
            sb.AppendLine("',')");
            sb.Append("as ");
            sb.Append(tempTableName);
            sb.Append(" where ");
            sb.Append(columnName);
            sb.Append(" = cast(");
            sb.Append(tempTableName);
            sb.Append(".value as ");
            sb.Append(set.Intervals.First().LowerBound < int.MinValue || set.Intervals.Last().UpperBound > int.MaxValue ? "bigint" : "int");
            sb.Append(")");
            sb.Append(")");
            return sb.ToString();
        }
    }
}
