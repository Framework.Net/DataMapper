using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;


public partial class clrTools // NOSONAR because putting this in a namespace "MyNamespace" would force sql user to write clrTools.MyNamespace.MethodName which is counter intuitive
{
    //https://docs.microsoft.com/fr-fr/sql/relational-databases/clr-integration-database-objects-user-defined-functions/clr-scalar-valued-functions?view=sql-server-2017
    //https://www.developer.com/net/net/article.php/11087_3550341_2/Writing-Database-Objects-in-CLR-Advanced-Scenarios.htm
    [SqlFunction(DataAccess = DataAccessKind.Read, IsDeterministic = true, IsPrecise = false, SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlInt64 ExecuteScalar(SqlChars conString, SqlChars request)
    {
        return _ExecuteScalarWithSkip(conString, request, new SqlInt32(0));
    }
    [SqlFunction(DataAccess = DataAccessKind.Read, IsDeterministic = true, IsPrecise = false, SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlInt64 ExecuteScalarWithSkip(SqlChars conString, SqlChars request, SqlInt32 interestingResultSetIndex)
    {
        return _ExecuteScalarWithSkip(conString, request, interestingResultSetIndex);
    }
    static SqlInt64 _ExecuteScalarWithSkip(SqlChars conString, SqlChars request, SqlInt32 interestingResultSetIndex)
    {
        if (interestingResultSetIndex.IsNull)
            interestingResultSetIndex = new SqlInt32(0);

        using (var con = new SqlConnection(new string(conString.Value)))
        using (var cmd = con.CreateCommand())
        {
            cmd.CommandText = new string(request.Value);
            con.Open();

            object res = null;
            var index = interestingResultSetIndex.Value;
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                do
                {
                    if (index-- == 0 && rdr.Read())
                        res = rdr.GetValue(0);
                    while (rdr.Read())
                    {
                        /* ignore */
                    }
                } while (rdr.NextResult());
            }
            return Convert.ToInt64(res);
        }
    }
}
