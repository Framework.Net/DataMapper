﻿using System;
using System.Collections.Generic;

using TechnicalTools.Model;


namespace DataMapper.Helpers
{
    public static class RemoteTuple
    {
        public static RemoteTuple<T1> Create<T1>(T1 item1)
        {
            return new RemoteTuple<T1>(item1);
        }
        public static RemoteTuple<T1, T2> Create<T1, T2>(T1 item1, T2 item2)
        {
            return new RemoteTuple<T1, T2>(item1, item2);
        }
        public static RemoteTuple<T1, T2, T3> Create<T1, T2, T3>(T1 item1, T2 item2, T3 item3)
        {
            return new RemoteTuple<T1, T2, T3>(item1, item2, item3);
        }
        public static RemoteTuple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(T1 item1, T2 item2, T3 item3, T4 item4)
        {
            return new RemoteTuple<T1, T2, T3, T4>(item1, item2, item3, item4);
        }
        public static RemoteTuple<T1, T2, T3, T4, T5> Create<T1, T2, T3, T4, T5>(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            return new RemoteTuple<T1, T2, T3, T4, T5>(item1, item2, item3, item4, item5);
        }
        public static RemoteTuple<T1, T2, T3, T4, T5, T6> Create<T1, T2, T3, T4, T5, T6>(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            return new RemoteTuple<T1, T2, T3, T4, T5, T6>(item1, item2, item3, item4, item5, item6);
        }
    }

    /// <summary>
    /// Class intended to be used with RemoteContext with datamapper.
    /// <example>
    /// var ids = SomeData.Select(data => new UniqueValue&lt;long&gt;(data.InterestingOperationId)).ToList();
    /// string tableIds;
    /// var context = PartnerEnv.Dao.CreateRemoteContext().With(ids, out tableIds);
    /// var operation = PartnerEnv.Dao.LoadCollectionFromInnerJoinSql&lt;OperationTypeMappedToDB>(context: context,
    ///                                  innerJoin: tableIds + " as I",
    ///                                  joinCondition: "A.Id = I.Item1", // A = name of main table containing OperationTypeMappedToDB
    ///                                  whereCondition: null)
    ///                               .ToList();
    /// </example>
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    public sealed class RemoteTuple<T1> : IAutoMappedDbObject
    {
        [DbMappedField("Item1", IsPK = true)] public T1 Item1 { get; set; }
        public RemoteTuple(T1 v)
        {
            Item1 = v;
        }

        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((RemoteTuple<T1>)source); }
        void ICopyable.CopyFrom(ICopyable source) { CopyFrom((RemoteTuple<T1>)source); }
        void CopyFrom(RemoteTuple<T1> source)
        {
            Item1 = source.Item1;
        }
    }

    /// <summary>
    /// Class intended to be used with RemoteContext with datamapper.
    /// For example, see <see cref="RemoteTuple{T}"/>
    /// </summary>
    public sealed class RemoteTuple<T1, T2> : IAutoMappedDbObject, IEquatable<RemoteTuple<T1, T2>>
    {
        [DbMappedField("Item1", IsPK = true, KeyOrderIndex = 1)] public T1 Item1 { get; set; }
        [DbMappedField("Item2", IsPK = true, KeyOrderIndex = 2)] public T2 Item2 { get; set; }
        public RemoteTuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((RemoteTuple<T1, T2>)source); }
        void ICopyable.CopyFrom(ICopyable source) { CopyFrom((RemoteTuple<T1, T2>)source); }
        void CopyFrom(RemoteTuple<T1, T2> source)
        {
            Item1 = source.Item1;
            Item2 = source.Item2;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(Item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(Item2);
                return hashCode;
            }
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as RemoteTuple<T1, T2>);
        }
        public bool Equals(RemoteTuple<T1, T2> other)
        {
            // EqualityComparer is the best way to compare value for both reference and value type, without boxing (it handles null value too)
            return EqualityComparer<T1>.Default.Equals(Item1, other.Item1)
                && EqualityComparer<T2>.Default.Equals(Item2, other.Item2);
        }
    }

    /// <summary>
    /// Class intended to be used with RemoteContext with datamapper.
    /// For example, see <see cref="RemoteTuple{T}"/>
    /// </summary>
    public sealed class RemoteTuple<T1, T2, T3> : IAutoMappedDbObject, IEquatable<RemoteTuple<T1, T2, T3>>
    {
        [DbMappedField("Item1", IsPK = true, KeyOrderIndex = 1)] public T1 Item1 { get; set; }
        [DbMappedField("Item2", IsPK = true, KeyOrderIndex = 2)] public T2 Item2 { get; set; }
        [DbMappedField("Item3", IsPK = true, KeyOrderIndex = 3)] public T3 Item3 { get; set; }
        public RemoteTuple(T1 item1, T2 item2, T3 item3)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
        }

        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((RemoteTuple<T1, T2, T3>)source); }
        void ICopyable.CopyFrom(ICopyable source) { CopyFrom((RemoteTuple<T1, T2, T3>)source); }
        void CopyFrom(RemoteTuple<T1, T2, T3> source)
        {
            Item1 = source.Item1;
            Item2 = source.Item2;
            Item3 = source.Item3;
        }


        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(Item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(Item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(Item3);
                return hashCode;
            }
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as RemoteTuple<T1, T2, T3>);
        }
        public bool Equals(RemoteTuple<T1, T2, T3> other)
        {
            // EqualityComparer is the best way to compare value for both reference and value type, without boxing (it handles null value too)
            return EqualityComparer<T1>.Default.Equals(Item1, other.Item1)
                && EqualityComparer<T2>.Default.Equals(Item2, other.Item2)
                && EqualityComparer<T3>.Default.Equals(Item3, other.Item3);
        }
    }

    /// <summary>
    /// Class intended to be used with RemoteContext with datamapper.
    /// For example, see <see cref="RemoteTuple{T}"/>
    /// </summary>
    public sealed class RemoteTuple<T1, T2, T3, T4> : IAutoMappedDbObject, IEquatable<RemoteTuple<T1, T2, T3, T4>>
    {
        [DbMappedField("Item1", IsPK = true, KeyOrderIndex = 1)] public T1 Item1 { get; set; }
        [DbMappedField("Item2", IsPK = true, KeyOrderIndex = 2)] public T2 Item2 { get; set; }
        [DbMappedField("Item3", IsPK = true, KeyOrderIndex = 3)] public T3 Item3 { get; set; }
        [DbMappedField("Item4", IsPK = true, KeyOrderIndex = 4)] public T4 Item4 { get; set; }
        public RemoteTuple(T1 item1, T2 item2, T3 item3, T4 item4)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
            Item4 = item4;
        }

        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((RemoteTuple<T1, T2, T3, T4>)source); }
        void ICopyable.CopyFrom(ICopyable source) { CopyFrom((RemoteTuple<T1, T2, T3, T4>)source); }
        void CopyFrom(RemoteTuple<T1, T2, T3, T4> source)
        {
            Item1 = source.Item1;
            Item2 = source.Item2;
            Item3 = source.Item3;
            Item4 = source.Item4;
        }


        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(Item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(Item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(Item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(Item4);
                return hashCode;
            }
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as RemoteTuple<T1, T2, T3, T4>);
        }
        public bool Equals(RemoteTuple<T1, T2, T3, T4> other)
        {
            // EqualityComparer is the best way to compare value for both reference and value type, without boxing (it handles null value too)
            return EqualityComparer<T1>.Default.Equals(Item1, other.Item1)
                && EqualityComparer<T2>.Default.Equals(Item2, other.Item2)
                && EqualityComparer<T3>.Default.Equals(Item3, other.Item3)
                && EqualityComparer<T4>.Default.Equals(Item4, other.Item4);
        }
    }

    /// <summary>
    /// Class intended to be used with RemoteContext with datamapper.
    /// For example, see <see cref="RemoteTuple{T}"/>
    /// </summary>
    public sealed class RemoteTuple<T1, T2, T3, T4, T5> : IAutoMappedDbObject, IEquatable<RemoteTuple<T1, T2, T3, T4, T5>>
    {
        [DbMappedField("Item1", IsPK = true, KeyOrderIndex = 1)] public T1 Item1 { get; set; }
        [DbMappedField("Item2", IsPK = true, KeyOrderIndex = 2)] public T2 Item2 { get; set; }
        [DbMappedField("Item3", IsPK = true, KeyOrderIndex = 3)] public T3 Item3 { get; set; }
        [DbMappedField("Item4", IsPK = true, KeyOrderIndex = 4)] public T4 Item4 { get; set; }
        [DbMappedField("Item5", IsPK = true, KeyOrderIndex = 5)] public T5 Item5 { get; set; }
        public RemoteTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
            Item4 = item4;
            Item5 = item5;
        }

        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((RemoteTuple<T1, T2, T3, T4, T5>)source); }
        void ICopyable.CopyFrom(ICopyable source) { CopyFrom((RemoteTuple<T1, T2, T3, T4, T5>)source); }
        void CopyFrom(RemoteTuple<T1, T2, T3, T4, T5> source)
        {
            Item1 = source.Item1;
            Item2 = source.Item2;
            Item3 = source.Item3;
            Item4 = source.Item4;
            Item5 = source.Item5;
        }


        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(Item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(Item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(Item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(Item4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(Item5);
                return hashCode;
            }
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as RemoteTuple<T1, T2, T3, T4, T5>);
        }
        public bool Equals(RemoteTuple<T1, T2, T3, T4, T5> other)
        {
            // EqualityComparer is the best way to compare value for both reference and value type, without boxing (it handles null value too)
            return EqualityComparer<T1>.Default.Equals(Item1, other.Item1)
                && EqualityComparer<T2>.Default.Equals(Item2, other.Item2)
                && EqualityComparer<T3>.Default.Equals(Item3, other.Item3)
                && EqualityComparer<T4>.Default.Equals(Item4, other.Item4)
                && EqualityComparer<T5>.Default.Equals(Item5, other.Item5);
        }
    }

    /// <summary>
    /// Class intended to be used with RemoteContext with datamapper.
    /// For example, see <see cref="RemoteTuple{T}"/>
    /// </summary>
    public sealed class RemoteTuple<T1, T2, T3, T4, T5, T6> : IAutoMappedDbObject, IEquatable<RemoteTuple<T1, T2, T3, T4, T5, T6>>
    {
        [DbMappedField("Item1", IsPK = true, KeyOrderIndex = 1)] public T1 Item1 { get; set; }
        [DbMappedField("Item2", IsPK = true, KeyOrderIndex = 2)] public T2 Item2 { get; set; }
        [DbMappedField("Item3", IsPK = true, KeyOrderIndex = 3)] public T3 Item3 { get; set; }
        [DbMappedField("Item4", IsPK = true, KeyOrderIndex = 4)] public T4 Item4 { get; set; }
        [DbMappedField("Item5", IsPK = true, KeyOrderIndex = 5)] public T5 Item5 { get; set; }
        [DbMappedField("Item6", IsPK = true, KeyOrderIndex = 6)] public T6 Item6 { get; set; }
        public RemoteTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
            Item4 = item4;
            Item5 = item5;
            Item6 = item6;
        }

        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyFrom((RemoteTuple<T1, T2, T3, T4, T5, T6>)source); }
        void ICopyable.CopyFrom(ICopyable source) { CopyFrom((RemoteTuple<T1, T2, T3, T4, T5, T6>)source); }
        void CopyFrom(RemoteTuple<T1, T2, T3, T4, T5, T6> source)
        {
            Item1 = source.Item1;
            Item2 = source.Item2;
            Item3 = source.Item3;
            Item4 = source.Item4;
            Item5 = source.Item5;
            Item6 = source.Item6;
        }


        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(Item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(Item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(Item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(Item4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(Item5);
                hashCode = (hashCode * 397) ^ EqualityComparer<T6>.Default.GetHashCode(Item6);
                return hashCode;
            }
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as RemoteTuple<T1, T2, T3, T4, T5, T6>);
        }
        public bool Equals(RemoteTuple<T1, T2, T3, T4, T5, T6> other)
        {
            // EqualityComparer is the best way to compare value for both reference and value type, without boxing (it handles null value too)
            return EqualityComparer<T1>.Default.Equals(Item1, other.Item1)
                && EqualityComparer<T2>.Default.Equals(Item2, other.Item2)
                && EqualityComparer<T3>.Default.Equals(Item3, other.Item3)
                && EqualityComparer<T4>.Default.Equals(Item4, other.Item4)
                && EqualityComparer<T5>.Default.Equals(Item5, other.Item5)
                && EqualityComparer<T6>.Default.Equals(Item6, other.Item6);
        }
    }

}
