﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Tools;

namespace DataMapper
{
    // Classe de base pour les objets qui sont en réalité des enums et qui peuvent evoluer lentement (Un autre dev ajoute une nouvelle valeur sans prévenir etc...)
    [DebuggerDisplay("{" + nameof(Caption) + ",nq}")]
    public abstract partial class DynamicEnum : BaseDTO, IComparable<DynamicEnum>, IComparable // CRTP pattern
    {
        public string Caption { get { return _Caption ?? EnumName; } protected set { _Caption = value; } } string _Caption;

        protected internal abstract long   EnumId   { get; set; }
        protected internal abstract string EnumName { get; set; }

        public override string ToString()
        {
            return EnumName;
        }

        public static long GetEnumId(DynamicEnum de) { return de.EnumId; }

        public static DynamicEnum GetValueFor(byte id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(sbyte id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(short id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(ushort id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(int id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(uint id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(long id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(ulong id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        public static DynamicEnum GetValueFor(string id, Type t, IDbMapper mapper) { return GetValueFor((object)id, t, mapper); }
        internal static DynamicEnum GetValueFor(object id, Type t, IDbMapper mapper)
        {
            if (!_toDynamicEnum.TryGetValue(t, out Func<object, DynamicEnum>  func))
            {
                // Note: At this point, _toDynamicEnum may have been replaced (race condition)!
                var newDico = new Dictionary<Type, Func<object, DynamicEnum>>(_toDynamicEnum);
                while (!t.IsGenericType || t.GetGenericTypeDefinition() != typeof(DynamicEnum<,>))
                    t = t.BaseType;
                var mGetValueFor = t.GetMethods(BindingFlags.Public | BindingFlags.Static)
                                    .First(mi => mi.Name == nameof(DummyEnumWithId.GetValueFor)
                                              && mi.GetParameters().Length == 2);
                var idType = mGetValueFor.GetParameters()[0].ParameterType;
                newDico[t] = func = str => (DynamicEnum)mGetValueFor.Invoke(null, new[] { Convert.ChangeType(str, idType), mapper });
                _toDynamicEnum = newDico;
            }
            return func(id);
        }
        // Lock free repository
        static Dictionary<Type, Func<object, DynamicEnum>> _toDynamicEnum = new Dictionary<Type, Func<object, DynamicEnum>>();

        public virtual int CompareTo(DynamicEnum other)
        {
            int res = string.Compare(GetType().FullName, other?.GetType().FullName, StringComparison.Ordinal);
            if (res != 0)
                return res;
            return string.Compare(Caption, other?.Caption, StringComparison.Ordinal);
        }

        public virtual int CompareTo(object obj)
        {
            if (obj is DynamicEnum dynEnum)
                return CompareTo(dynEnum);
            return Caption.CompareTo(obj);
        }

        [AttributeUsage(AttributeTargets.Property)]
        protected class DbNameBoundAttribute : Attribute
        {
            public string NameInDB { get; }
            public string Caption  { get; set; }

            public DbNameBoundAttribute(string nameInDb)
            {
                NameInDB = nameInDb;
            }
        }

        protected internal DynamicEnum()
        {
        }

        [Browsable(false)]
        [IsTechnicalProperty]
        public abstract Type KeyType { get; }
    }
    public abstract class DynamicEnum<TKey, TEnumClass> : DynamicEnum, IComparable // IComparable is needed again to override explicite call
        where TKey : struct, IConvertible
        where TEnumClass : DynamicEnum<TKey, TEnumClass>, IAutoMappedDbObject, new()
    {
        /// <summary> Allow developper to get an instance/value of this dynamic enum from an id.</summary>
        public static TEnumClass GetValueFor(TKey id)
        {
            return GetValueFor(id, null);
        }
        /// <summary> Allow developper to get an instance/value of this dynamic enum from an id on the dynamic enum pool of a specific mapper.</summary>
        public static TEnumClass GetValueFor(TKey id, IDbMapper mapper)
        {
            return _GetValueFor(id, mapper);
        }
        // For internal reflection
        // Note : We wrote two methods above so they can be used in Expression Tree
        // (Expression tree does not like call to method with optional argument...)
        internal static TEnumClass _GetValueFor(TKey id, IDbMapper mapper)
        {
            CheckHasBeenInitialized(mapper);
            var b = mapper == null ? null : new SqlConnectionStringBuilder(mapper.GetConnectionString());
            var key = b == null ? null : Tuple.Create(b.DataSource, b.InitialCatalog);
            var forAType = _allValuesByIdsByDeclaringTypes[typeof(TEnumClass)];
            var forATypeAndOrigin = key == null
                                  ? forAType?.SingleOrDefault().Value
                                  : forAType[key];
            if (forATypeAndOrigin == null)
                throw new Exception($"No values for type {typeof(TEnumClass)} and {key}!");
            if (forATypeAndOrigin.TryGetValue(id, out TEnumClass value))
                return value;
            value = new TEnumClass
            {
                EnumId = Convert.ToInt32(id),
                EnumName = $"<Unknown name for {id}>"
            };
            forATypeAndOrigin.TryAdd(id, value);
            return value;
        }

        public override Type KeyType { get { return typeof(TKey); } }

        // Allow developper to enumerate all enum values recognised
        public static ICollection<TEnumClass> All { get { return GetAll(null); } }
        public static ICollection<TEnumClass> GetAll(IDbMapper mapper = null)
        {
            CheckHasBeenInitialized(mapper);
            var forAType = _allValuesByIdsByDeclaringTypes[typeof(TEnumClass)];
            if (mapper != null)
            {
                var b = new SqlConnectionStringBuilder(mapper.GetConnectionString());
                return forAType[Tuple.Create(b.DataSource, b.InitialCatalog)].Values;
            }
            if (forAType.Count == 1)
                return forAType.First().Value.Values;
            throw new Exception("Ambiguous values to fetch, precise origin by supplying a dbMapper", null);
        }

        public static void RecogniseExplicitNamedEnumValues(IDbMapper mapper, bool asDisconnected)
        {
            RecogniseExplicitNamedEnumValues<TEnumClass>(mapper, asDisconnected);
        }
        public static void RecogniseExplicitNamedExtendedEnumValues<TExtendedEnumClass>(IDbMapper mapper, bool asDisconnected)
            where TExtendedEnumClass : TEnumClass
        {
            RecogniseExplicitNamedEnumValues<TExtendedEnumClass>(mapper, asDisconnected);
        }
        static void RecogniseExplicitNamedEnumValues<TExtendedEnumClass>(IDbMapper mapper, bool asDisconnected)
            where TExtendedEnumClass : TEnumClass
        {
            var declaringType = typeof(TExtendedEnumClass);
            var forAType = _allValuesByIdsByDeclaringTypes.GetValueOrCreateDefault(declaringType);
            var b = asDisconnected ? new SqlConnectionStringBuilder(string.Empty) : new SqlConnectionStringBuilder(mapper.GetConnectionString());
            var forATypeAndOrigin = forAType.GetValueOrCreateDefault(Tuple.Create(b.DataSource, b.InitialCatalog));
            DebugTools.Assert(forATypeAndOrigin.Count == 0, "Init already called !");
            if (declaringType.GetCustomAttribute<DbNotMappedEnumAttribute>() != null)
            {
                foreach (var pm in StaticProperties(declaringType).Select(propInfo => (TEnumClass)propInfo.GetValue(null)))
                    forATypeAndOrigin.TryAdd((TKey)Convert.ChangeType(pm.EnumId, typeof(TKey)), pm);
                return;
            }
            // Load all values in data base
            var baseList = _allValuesByIdsByDeclaringTypes.TryGetValueClass(typeof(TEnumClass))?.TryGetValueClass(Tuple.Create(b.DataSource, b.InitialCatalog));
            ConcurrentDictionary<TKey, TEnumClass> allValuesByIds;
            long disconnectedSeed = 0;
            if (typeof(TExtendedEnumClass) == typeof(TEnumClass) || baseList == null)
                if (asDisconnected)
                {
                    var cons = DefaultObjectFactory<TExtendedEnumClass>.Constructor;
                    allValuesByIds = MappedStaticProperties(declaringType)
                                        .Select(p => p.GetCustomAttribute<DbNameBoundAttribute>())
                                        .NotNull()
                                        .Select(att =>
                                        {
                                            var v = (TEnumClass)cons();
                                            v.EnumId = ++disconnectedSeed;
                                            v.EnumName = att.NameInDB;
                                            return v;
                                        })
                                        .ToConcurrentDictionary(pm => (TKey)Convert.ChangeType(pm.EnumId, typeof(TKey)));
                }
                else
                    allValuesByIds = mapper.LoadCollection<TEnumClass>().ToConcurrentDictionary(pm => (TKey)Convert.ChangeType(pm.EnumId, typeof(TKey)));
            else
                allValuesByIds = baseList;

            var not_mapped_by_properties = new List<PropertyInfo>();

            // Assign static properties with loaded values

            foreach (var p in MappedStaticProperties(declaringType))
            {
                var att = p.GetCustomAttribute<DbNameBoundAttribute>();
                if (att == null)
                    continue;
                var enumValueName = att.NameInDB;
                var enumValue = allValuesByIds.Select(kvp => kvp.Value)
                                              .FirstOrDefault(ev => ev.EnumName == enumValueName);
                if (enumValue != null)
                {
                    enumValue.Caption = att.Caption; // developper want to customize enum caption ?
                    p.SetValue(null, enumValue);
                }
                else
                    not_mapped_by_properties.Add(p);
            }

            forAType = _allValuesByIdsByDeclaringTypes.GetOrAdd(typeof(TExtendedEnumClass), _ => new ConcurrentDictionary<Tuple<string, string>, ConcurrentDictionary<TKey, TEnumClass>>());
            forATypeAndOrigin = forAType.GetOrAdd(Tuple.Create(b.DataSource, b.InitialCatalog), _ => new ConcurrentDictionary<TKey, TEnumClass>());
            foreach (var v in allValuesByIds)
                forATypeAndOrigin.TryAdd(v.Key, v.Value);

            #region  Checking the match is perfect and display it as an assertion
            string inDatabase = null;
            string inCode = null;
            if (not_mapped_by_properties.Count > 0)
            {
                inCode = string.Join(Environment.NewLine,
                                     not_mapped_by_properties.Select(p => typeof(TEnumClass).Name + "." + p.Name + Environment.NewLine +
                                                                          "   mapped with \"" + p.GetCustomAttribute<DbNameBoundAttribute>()?.NameInDB + "\""));
                if (!string.IsNullOrWhiteSpace(inCode))
                {
                    inCode = "YOU HAVE TO FIX this as soon as possible." + Environment.NewLine +
                             Environment.NewLine + Environment.NewLine + Environment.NewLine +
                             $"Objets \"{typeof(TEnumClass).Name}\" dans le code, non existant en base (mapping cassé ?) :" + Environment.NewLine +
                             inCode;
                }
            }
            if (allValuesByIds.Count > AllExpectedById(declaringType).Count)
            {
                inDatabase = string.Join(Environment.NewLine, allValuesByIds.Values.Except(AllExpectedById(declaringType).Values).Select(f => " - \"" + f.EnumName + "\""));
                if (!string.IsNullOrWhiteSpace(inDatabase))
                {
                    inDatabase = $"Objets \"{typeof(TEnumClass).Name}\" présents en base... mais non mappés dans le code :" + Environment.NewLine +
                                 inDatabase;
                    if (!string.IsNullOrWhiteSpace(inCode))
                        inDatabase = $"This change is not a problem for {Assembly.GetEntryAssembly().GetName().Name}. This message is just a reminder to synchronize manually the enumeration!" +
                                     Environment.NewLine + Environment.NewLine +
                                     inDatabase;
                }
            }
            string msg = string.Join(Environment.NewLine + Environment.NewLine, new[] { inCode, inDatabase }.Where(str => !string.IsNullOrWhiteSpace(str)));
            if (!string.IsNullOrWhiteSpace(inCode))
                DebugTools.Assert(msg.Length == 0, msg.Trim()); // Obviously the assert will fail but we need an assert here
            else
            {
                // if assert fails, it is not a big deal in this case (see generated message to understand)
                // You can disable this kind of assert by using attribute DbNameBoundAreIncompleteAttribute on class
                DebugTools.Should(msg.Length == 0 ||
                                 (typeof(TExtendedEnumClass).GetCustomAttribute<GeneratedClassIsDynamicEnumAttribute>()?.ListCanBeExtendedInDatabase ?? false),
                                 msg.Trim());
            }
            #endregion  Checking the match is perfect and display it as an assertion
        }
        internal static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Tuple<string, string>, ConcurrentDictionary<TKey, TEnumClass>>> _allValuesByIdsByDeclaringTypes = new ConcurrentDictionary<Type, ConcurrentDictionary<Tuple<string, string>, ConcurrentDictionary<TKey, TEnumClass>>>();

        internal static void AddNewItem(TEnumClass item, IDbMapper mapper)
        {
            var b = new SqlConnectionStringBuilder(mapper.GetConnectionString());
            var _allValues = _allValuesByIdsByDeclaringTypes.TryGetValueClass(typeof(TEnumClass));
            var _allValuesById = _allValues?.TryGetValueClass(Tuple.Create(b.DataSource, b.InitialCatalog));
            var key = (TKey)Convert.ChangeType(item.EnumId, typeof(TKey));
            if (!_allValuesById.TryGetValue(key, out TEnumClass dummy))
                _allValuesById.TryAdd(key, item);
        }

        static void CheckHasBeenInitialized(IDbMapper mapper)
        {
            var _allValues = _allValuesByIdsByDeclaringTypes.TryGetValueClass(typeof(TEnumClass));
            bool present;
            if (mapper != null)
            {
                var b = new SqlConnectionStringBuilder(mapper.GetConnectionString());
                present = _allValues?.ContainsKey(Tuple.Create(b.DataSource, b.InitialCatalog)) ?? false;
            }
            else
                present = (_allValues?.Count ?? 0) > 0;

            if (!present && typeof(TEnumClass).GetCustomAttribute<DbNotMappedEnumAttribute>() == null)
                throw new Exception(nameof(RecogniseExplicitNamedEnumValues) + " on class " + typeof(TEnumClass).Name + " must be called before using instances of this classe or static features!");
        }

        //public override int CompareTo(object other)
        //{
        //    if (other is TEnumClass)
        //        return CompareTo(other as TEnumClass);
        //    if (other is string)
        //        return CompareTo(other as string);
        //    return base.CompareTo(other);
        //}
        //public override int CompareTo(DynamicEnum other)
        //{
        //    base.CompareTo(other);
        //    return var res = GetType().Name.CompareTo(other.GetType().Name);
        //    if (res != 0)
        //        return res;
        //    return Caption.CompareTo(other.Caption);
        //}
        //public int CompareTo(TEnumClass other)
        //{
        //    return Caption.CompareTo(other?.Caption);
        //}
        //public int CompareTo(string caption)
        //{
        //    return Caption.CompareTo(caption);
        //}

        static List<PropertyInfo> StaticProperties(Type declaringType)
        {
            return declaringType.GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly)
                                .Where(p => p.PropertyType == typeof(TEnumClass) &&
                                            p.DeclaringType == declaringType) // avoid inherited & already initialiazed properties
                                .ToList();
        }

        static List<PropertyInfo> MappedStaticProperties(Type declaringType)
        {
            return StaticProperties(declaringType)
                        .Where(p => null != p.GetCustomAttribute<DbNameBoundAttribute>())
                        .ToList();
        }

        static Dictionary<TKey, TEnumClass> AllExpectedById(Type declaringType)
        {
            return MappedStaticProperties(declaringType)
                        .Select(p => (TEnumClass)p.GetValue(null, null))
                        .Where(value => value != null) // Enum Value could be null if not matched in database !
                        .ToDictionary(pm => (TKey)Convert.ChangeType(pm.EnumId, typeof(TKey)));
        }
    }
}
