﻿using System;


namespace DataMapper
{
    public struct RangeOf<T>
        where T : IComparable<T>, IConvertible
    {
        public T Min { get; private set; }
        public T Max { get; private set; }

        public RangeOf(T min, T max)
        {
            Min = min;
            Max = max;
        }
    }
}
