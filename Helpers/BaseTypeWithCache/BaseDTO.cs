﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model.Cache;


namespace DataMapper
{
    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    public abstract class BaseDTO<TIdTuple> : BaseDTO, IHasTypedIdReadable<TIdTuple>, ICloneable
        where TIdTuple : struct, IEquatable<TIdTuple>, IIdTuple
    {
        #region Same implementation than DynamicEnum (in the same folder)
        /// <summary>
        /// Représente l'id de l'objet. L'id est un IIdTuple (composition de plusieurs données de type simple : int string date etc)
        /// Cette proprieté est nommé ClosedId pour eviter un conflit de nom avec la propriete courrament nommée Id dans les classes filles
        /// </summary>
        protected internal abstract TIdTuple ClosedId { get; set; } IIdTuple IHasClosedIdReadable.Id { get { return ClosedId; } }

        [Browsable(false)][IsTechnicalProperty]
        public virtual TypedId<TIdTuple>                               TypedId                  { get { return new TypedId<TIdTuple>(GetType(), ClosedId); } }
                       ITypedId          IHasTypedIdReadable          .TypedId                  { get { return TypedId; } }
                       ITypedId          IHasTypedIdReadable          .MakeTypedId(IIdTuple id) { return id.ToTypedId(GetType()); }
                       TypedId<TIdTuple> IHasTypedIdReadable<TIdTuple>.MakeTypedId(TIdTuple id) { return (TypedId<TIdTuple>)id.ToTypedId(GetType()); }

        public BaseDTO<TIdTuple>            Clone(bool copyIdentity) { var clone = (BaseDTO<TIdTuple>)Clone(); clone.ClosedId = ClosedId; clone.OwnerMapper = OwnerMapper; return clone; }
        object                   ICloneable.Clone()                  { return Clone(true); }

        public override int GetHashCode()
        {
            if (TechnicalTools.Model.Cache.TypedId.TypesWithInteger.Contains(TypedId.GetType()) &&
                Convert.ToInt64(TypedId.Key.Keys[0]) == 0)
                // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
                return base.GetHashCode();
            return TypedId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is BaseDTO<TIdTuple> asBaseDTOWithId))
                return false;
            return TypedId.Equals(asBaseDTOWithId.TypedId)  // ok object match but ...
                && ReferenceEquals(OwnerMapper?.Owner, asBaseDTOWithId.OwnerMapper?.Owner); // ... do they belongs to the same owner (ie: in general it means same database) ?
        }

        string AsDebugString
        {
            get
            {
                return "("
                    + (OwnerMapper?.Owner?.Name
                        ?? "<NO SPECIFIC OWNER>")
                    + ", "
                    + ObjectDebugString
                    + ")";
            }
        }
        protected virtual string ObjectDebugString
        {
            get
            {
                return TypedId.ToSmartString();
            }
        }

        #endregion

        // Provides a class for a collection of items whose keys are embedded in the values.
        // This is equivalent to a HashSet with an equality Comparer on ClosedId
        public class KeyedCollection<TObject> : System.Collections.ObjectModel.KeyedCollection<TIdTuple, TObject>
            where TObject : BaseDTO<TIdTuple>
        {
            protected override TIdTuple GetKeyForItem(TObject item)
            {
                return item.ClosedId;
            }

            public TObject this[TObject key] { get { return this[key.ClosedId]; } }
            [Obsolete("Probably not the override you want to call...", true)]
            public new TObject this[int key] { get { return base[key]; } }


        }
    }
}
