﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace DataMapper
{
    public interface IBaseDTO : IAutoMappedDbObject, ICloneable, ICopyable<BaseDTO>//, ICanMask
    {
    }

    // Classe de base pour objets servant de DTO, afin de fournir des méthodes de check
    public abstract partial class BaseDTO : IBaseDTO
    {
        [IsTechnicalProperty]
        [Browsable(false)]
        [XmlIgnore]
        public virtual IDbMapper OwnerMapper { get; protected internal set; }

        protected BaseDTO()
        {
            if (TrackInstances)
                ObjectTracker.Instance.Track(this);
        }
        public static bool TrackInstances { get; set; }

        // Pour dans l'avenir gérer les notifications de champs business
        protected void SetProperty<T>(ref T backingField, T value)
        {
            backingField = value;
        }

        // Pour dans l'avenir gérer les notifications de champs technique
        protected void SetTechnicalProperty<T>(ref T backingField, T value, bool raisedIdChanged)
        {
            backingField = value;
        }
        protected void SetTechnicalProperty<T>(ref T backingField, T value, Action onChange = null)
        {
            backingField = value;
        }

        protected void RaiseIdChanged()
        {
            // Pour dans l'avenir gérer les notifications de modification d'ID
        }

        protected internal static T GetValueAs<T>(object value)
        {
            if (value is DBNull)
            {
                if (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>))
                    return (T)(object)null;
                if (!typeof(T).IsValueType)
                    return (T)(object)null;
            }
            return (T)value;
        }

        #region  Checks

        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkFieldLen(ref string str, int maxLen, [CallerMemberName] string propertyName = null)
        {
            if (str != null && str.Length > maxLen)
            {
                string msg = GetType().Name + "." + (propertyName ?? "???") + " is limited to " + maxLen + " characters. Cannot set the following value :" + Environment.NewLine + str;
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintMaximumLength(msg, str.Length, maxLen);
                else
                    DebugTools.Assert(false, msg);
            }
        }
        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkFieldLen(ref string str, int minLen, int maxLen, [CallerMemberName] string propertyName = null)
        {
            if (str != null && str.Length < minLen)
            {
                string msg = GetType().Name + "." + (propertyName ?? "???") + " must be at least " + maxLen + " characters long. Cannot set the following value :" + Environment.NewLine + str;
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintMinimumLength(msg, str.Length, maxLen);
                else
                    DebugTools.Assert(false, msg);
            }
            ChkFieldLen(ref str, maxLen, propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkFieldLen(ref byte[] array, int maxLen, [CallerMemberName] string propertyName = null)
        {
            if (array != null && array.Length > maxLen)
            {
                string msg = GetType().Name + "." + (propertyName ?? "???") + " is limited to " + maxLen + " bytes. The value to be set is " + array.Length + " bytes long." + Environment.NewLine + array.Join();
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintMaximumLength(msg, array.Length, maxLen);
                else
                    DebugTools.Assert(false, msg);
            }
        }
        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkRange(DateTime? dt, DateTime min, DateTime max, [CallerMemberName] string propertyName = null)
        {
            _ChkRange(dt, min, max, true, propertyName ?? "???");
        }
        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkRange(DateTime dt, DateTime min, DateTime max, [CallerMemberName] string propertyName = null)
        {
            _ChkRange(dt, min, max, false, propertyName ?? "???");
        }
        [DebuggerHidden, DebuggerStepThrough]
        void _ChkRange(DateTime? dt, DateTime min, DateTime max, bool isNullable, string propertyName)
        {
            if (dt.HasValue)
            {
                if (dt.Value < min)
                {
                    string msg = GetType().Name + "." + propertyName + " technical minimal allowed value is " + min.ToString("o") + ". Cannot set the following value :" + Environment.NewLine + dt.Value.ToString("o");
                    if (DebugTools.IsForDevelopper)
                        throw new TechnicalDbConstraintMinimalValue(msg, dt.Value, min);
                    else
                        DebugTools.Assert(false, msg);
                }
                if (dt.Value > max)
                {
                    string msg = GetType().Name + "." + propertyName + " technical maximal allowed value is " + max.ToString("o") + ". Cannot set the following value :" + Environment.NewLine + dt.Value.ToString("o");
                    if (DebugTools.IsForDevelopper)
                        throw new TechnicalDbConstraintMaximalValue(msg, dt.Value, max);
                    else
                        DebugTools.Assert(false, msg);
                }
            }
            else if (!isNullable)
            {
                string msg = GetType().Name + "." + propertyName + " is not nullable.";
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintNotNullable(msg);
                else
                    DebugTools.Assert(false, msg);
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkRange<T>(T dt, RangeOf<T> range, [CallerMemberName] string propertyName = null)
            where T : struct, IComparable<T>, IConvertible
        {
            _ChkRange(dt, range, false, propertyName ?? "???");
        }
        [DebuggerHidden, DebuggerStepThrough]
        protected void ChkRange<T>(T? dt, RangeOf<T> range, [CallerMemberName] string propertyName = null)
            where T : struct, IComparable<T>, IConvertible
        {
            _ChkRange(dt, range, true, propertyName ?? "???");
        }
        [DebuggerHidden, DebuggerStepThrough]
        void _ChkRange<T>(T? dt, RangeOf<T> range, bool isNullable, string propertyName)
            where T : struct, IComparable<T>, IConvertible
        {
            if (dt.HasValue)
            {
                if (dt.Value.CompareTo(range.Min) < 0)
                {
                    string msg = GetType().Name + "." + propertyName + " technical minimal allowed value is " + range.Min.ToStringInvariant() + ". Cannot set the following value :" + Environment.NewLine + dt.Value.ToStringInvariant();
                    if (DebugTools.IsForDevelopper)
                        throw new TechnicalDbConstraintMinimalValue(msg, dt, range.Min);
                    else
                        DebugTools.Assert(false, msg);
                }
                if (dt.Value.CompareTo(range.Max) > 0)
                {
                    string msg = GetType().Name + "." + propertyName + " technical maximal allowed value is " + range.Max.ToStringInvariant() + ". Cannot set the following value :" + Environment.NewLine + dt.Value.ToStringInvariant();
                    if (DebugTools.IsForDevelopper)
                        throw new TechnicalDbConstraintMaximalValue(msg, dt.Value, range.Max);
                    else
                        DebugTools.Assert(false, msg);
                }
            }
            else if (!isNullable)
            {
                string msg = GetType().Name + "." + propertyName + " is not nullable.";
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintNotNullable(msg);
                else
                    DebugTools.Assert(false, msg);
            }
        }

        protected void CheckDateTimePrecision(DateTime? dt, bool isNullable, bool mustNotHavePartTime, byte numberDecimalAfterSecondsAllowed, [CallerMemberName] string propertyName = null)
        {
            Debug.Assert(numberDecimalAfterSecondsAllowed <= 7); // 0 => Time part is allowed, but only at a second resolution scale
            if (!dt.HasValue)
            {
                if (isNullable)
                    return;
                string msg = GetType().Name + "." + propertyName + " is not nullable.";
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintNotNullable(msg);
                else
                    DebugTools.Assert(false, msg);
            }
            else if (mustNotHavePartTime)
            {
                if (dt.Value.TimeOfDay != TimeSpan.Zero)
                {
                    string msg = GetType().Name + "." + propertyName + " date is not allowed to contains time part (the value setted is : " + dt + ")";
                    if (DebugTools.IsForDevelopper)
                        throw new TechnicalDbConstraintDateTimeHaveToMuchPrecision(msg, dt.Value, false, 0);
                    else
                        DebugTools.Assert(false, msg);
                }
            }
            else if (dt.Value.Ticks % TimePartPrecisionsOfDate[numberDecimalAfterSecondsAllowed] != 0)
            {
                string msg = GetType().Name + "." + propertyName + " date has a time part too much accurate for storage (the value setted is : " + dt + ") and only " + numberDecimalAfterSecondsAllowed + " digits can be stored";
                if (DebugTools.IsForDevelopper)
                    throw new TechnicalDbConstraintDateTimeHaveToMuchPrecision(msg, dt.Value, false, (byte)numberDecimalAfterSecondsAllowed);
                else
                    DebugTools.Assert(false, msg);
            }
        }
        static readonly int[] TimePartPrecisionsOfDate = new [] {10000000, 1000000, 100000, 10000, 1000, 100, 10, 1};
        #endregion

        #region Cloneable (Implementation de ICloneable permettant de gerer l'heritage plus tard)

        object ICloneable.Clone()
        {
            return Clone();
        }
        protected BaseDTO Clone()
        {
            var clone = CreateNewInstance();
            clone.CopyAllFieldsFrom(this);
            return clone;
        }
        protected abstract BaseDTO CreateNewInstance();
        public abstract void CopyAllFieldsFrom(BaseDTO source);

        void ICopyable.CopyFrom(ICopyable source) { CopyAllFieldsFrom((BaseDTO)source); }
        void ICopyable<BaseDTO>.CopyFrom(BaseDTO source) { CopyAllFieldsFrom(source); }
        void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source) { CopyAllFieldsFrom((BaseDTO)source); }

        #endregion
    }
}
