﻿using System;

using TechnicalTools.Model.Cache;

namespace DataMapper
{
    // Just some implementation to be able to pick nameof of
    public abstract partial class DynamicEnum
    {
        public class DummyEnum : DynamicEnum<long, DummyEnum>, IAutoMappedDbObject
        {
            protected internal override long EnumId { get; set; }
            protected internal override string EnumName { get; set; }
            public override void CopyAllFieldsFrom(BaseDTO source) { throw new InvalidOperationException(); }
            protected override BaseDTO CreateNewInstance() { return null; }
        }
        public class DummyEnumWithId : DynamicEnumWithId<long, DummyEnumWithId>, IAutoMappedDbObject
        {
            protected override IdTuple<long> ClosedId { get; set; }
            protected internal override long EnumId { get; set; }
            protected internal override string EnumName { get; set; }
            public override void CopyAllFieldsFrom(BaseDTO source) { throw new InvalidOperationException(); }
            protected override BaseDTO CreateNewInstance() { return null; }
        }
    }
}
