﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMapper
{
    public interface IHasOptimizedLoading
    {
        void FillFromReader(System.Data.Common.DbDataReader reader);
    }
}
