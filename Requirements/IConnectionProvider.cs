﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataMapper
{
    public interface IConnectionProvider : IDisposable
    {
        string ConnectionString { get; }

        IEnumerable<DbDataReader> GetEnumerableReader(string sql, RemoteContext context = null);

        DataTable GetDataTable(string sql, RemoteContext context = null);

        IDbTransaction StartTransaction();

        int ExecuteNonQuery(string sql);

        T      ExecuteScalar<T>(string sql, bool canRetry, T default_value = default(T)) where T : struct;
        object ExecuteScalar   (string sql, bool canRetry, object default_value = null);

        void WithConnectionDo(Action<SqlConnection, IDbTransaction> doWithRealConnection);
    }
}
