﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DataMapper;

namespace DataMapper.Tools
{
    // From http://www.developerfusion.com/article/122498/using-sqlbulkcopy-for-high-performance-inserts/
    public class MappedDbObjectDataReader<TObject> : IDataReader
        where TObject : IAutoMappedDbObject
    {
        readonly DbMappedTable        _mtable;
                 IEnumerator<TObject> _enumerator;

        public MappedDbObjectDataReader(DbMappedTable mtable, IEnumerable<TObject> items)
        {
            _mtable =     mtable                 ?? throw new ArgumentException("Mapping table cannot be null!", nameof(mtable));
            _enumerator = items?.GetEnumerator() ?? throw new ArgumentException("Items must be enumerable!",     nameof(items));
        }

        bool IDataReader.Read()
        {
            if (_enumerator == null)
                throw new ObjectDisposedException("ObjectDataReader");
            return _enumerator.MoveNext();
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_enumerator != null)
                {
                    _enumerator.Dispose();
                    _enumerator = null;
                }
            }
        }

        void IDataReader.Close()
        {
            Dispose();
        }
        bool IDataReader.IsClosed
        {
            get { return _enumerator == null; }
        }


        int IDataRecord.FieldCount
        {
            get
            {
                return _mtable.AllFields.Count - (_mtable.AutoIncrementedField == null ? 0 : 1);
            }
        }



        int IDataRecord.GetOrdinal(string columnName)
        {
            var field = _mtable.AllFieldsByColumnName[columnName];
            return field.OrdinalIndex;
        }
        object IDataRecord.GetValue(int i)
        {
            var field = _mtable.AllFields[i];
            Debug.Assert((this as IDataRecord).GetOrdinal(field.ColumnName) == i);
            if (_enumerator == null)
                throw new ObjectDisposedException("ObjectDataReader");
            var result = field.GetValueDirect(_enumerator.Current);
            if (result is DynamicEnum)
                return (result as DynamicEnum).EnumId;
            return result;
        }

        #region Useless methods for what this class is intended

        int         IDataReader.Depth                       { get { throw new NotImplementedException(); } }
        int         IDataReader.RecordsAffected             { get { throw new NotImplementedException(); } }
        object      IDataRecord.this[string name]           { get { throw new NotImplementedException(); } }
        object      IDataRecord.this[int i]                 { get { throw new NotImplementedException(); } }

        DataTable   IDataReader.GetSchemaTable()            { throw new NotImplementedException(); }

        bool        IDataReader.NextResult()                { throw new NotImplementedException(); }
        string      IDataRecord.GetName(int i)              { throw new NotImplementedException(); }
        string      IDataRecord.GetDataTypeName(int i)      { throw new NotImplementedException(); }
        Type        IDataRecord.GetFieldType(int i)         { throw new NotImplementedException(); }
        int         IDataRecord.GetValues(object[] values)  { throw new NotImplementedException(); }
        bool        IDataRecord.GetBoolean(int i)           { throw new NotImplementedException(); }
        byte        IDataRecord.GetByte(int i)              { throw new NotImplementedException(); }
        long        IDataRecord.GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
        char        IDataRecord.GetChar(int i)              { throw new NotImplementedException(); }
        long        IDataRecord.GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
        Guid        IDataRecord.GetGuid(int i)              { throw new NotImplementedException(); }
        short       IDataRecord.GetInt16(int i)             { throw new NotImplementedException(); }
        int         IDataRecord.GetInt32(int i)             { throw new NotImplementedException(); }
        long        IDataRecord.GetInt64(int i)             { throw new NotImplementedException(); }
        float       IDataRecord.GetFloat(int i)             { throw new NotImplementedException(); }
        double      IDataRecord.GetDouble(int i)            { throw new NotImplementedException(); }
        string      IDataRecord.GetString(int i)            { throw new NotImplementedException(); }
        decimal     IDataRecord.GetDecimal(int i)           { throw new NotImplementedException(); }
        DateTime    IDataRecord.GetDateTime(int i)          { throw new NotImplementedException(); }
        IDataReader IDataRecord.GetData(int i)              { throw new NotImplementedException(); }
        bool        IDataRecord.IsDBNull(int i)             { return false; }

        #endregion Useless methods for what this class is intended
    }
}