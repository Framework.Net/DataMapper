﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Logs;


namespace DataMapper.Tools
{
    public static class SqlCallsLogging
    {
        public static void OnSqlCommandRequest(ILogger log, SqlCommand query, bool hasSideEffect, Action queryExecution)
        {
            OnSqlCommandRequest(log, query, hasSideEffect, () => { queryExecution(); return false; });
        }
        public static T OnSqlCommandRequest<T>(ILogger log, SqlCommand query, bool hasSideEffect, Func<T> queryExecution)
        {
            var sw = new Stopwatch();
            try
            {
                sw.Start();
                T res = queryExecution();
                sw.Stop();
                // Log here because in case of exception we put data in Exception object (which is logged later)
                string logMsg = "Sql command done in (" + sw.ElapsedMilliseconds + " ms): " + ToSqlExecutableCode(query);
                if (hasSideEffect)
                    log.Audit(logMsg);
                else
                    log.Info(logMsg);
                return res;
            }
            // This catch never handles exception, we just use when clause to bind some code to event
            catch (Exception ex) when (_OnException_StopPerformanceWatcher_AttachSqlQueryToException(ex, query, sw)) { throw; }
        }

        // Static method is faster than lambda
        static bool _OnException_StopPerformanceWatcher_AttachSqlQueryToException(Exception ex, SqlCommand cmd, Stopwatch sw)
        {
            sw.Stop();
            TryAttachSqlQueryOnException(ex, cmd, sw);
            return false;
        }
        internal static Exception TryAttachSqlQueryOnException(this Exception ex, SqlCommand cmd, Stopwatch stoppedSw = null)
        {
            string query = ToSqlExecutableCode(cmd);
            ex.EnrichDiagnosticWith("Sql command failed (in " + stoppedSw.ElapsedMilliseconds + " ms): " + Environment.NewLine + query, eExceptionEnrichmentType.Technical);
            return ex;
        }
        internal static string ToSqlExecutableCode(SqlCommand cmd)
        {
            try
            {
                // Sometimes throw exception when type is stored procedure
                return cmd.CommandAsSql();
            }
            catch (Exception ex)
            {
                // Too bad... we do nothing and ignore because
                // this method is for handling exception
                return "EXEC " + cmd.CommandText + cmd.Parameters.Cast<SqlParameter>().Select(p => "@" + p.ParameterName + " = " + DbMappedField.ToSql(p.Value)).Join()
                     + Environment.NewLine
                     + "Note that we cannot generate sql because: " + Environment.NewLine + ex.ToString();
            }
        }


    }
}
